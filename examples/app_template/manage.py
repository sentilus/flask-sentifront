"""
.. module:: manage
    :synopsis: An implementation of flask_script for invoking app commands

.. moduleauthor:: Sentilus Associates
"""

import sys
from os import path
from flask.ext.script import Manager
from werkzeug.serving import run_simple
from flask_reporting.collector import populate_analysis_reports
from flask.ext.migrate import Migrate, MigrateCommand

from base_app.extensions import db, mail, login_manager, reporting, FlaskCelery
from base_app.app import create_app, setup_app
from config import DebugConfig, BaseConfig
from db_scripts import populate_users, drop_everything

app = create_app(config=DebugConfig)
app = setup_app(app)

manager = Manager(app)
manager.add_option('-c', '--config',
                   dest="config",
                   required=False,
                   help="config file")

# Migrations
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)

celery_app = FlaskCelery(app=app, include='base_app.tasks_async')


@manager.command
def run():

    """Run in local machine. """

    run_simple('0.0.0.0', 5000, app,
               use_reloader=True, use_debugger=True, use_evalex=True)


@manager.command
def init_db():

    """Initialise the database. """

    with app.app_context():
        db.create_all()
        populate_users(app, db)


@manager.command
def drop_all():

    with app.app_context():
        drop_everything(db)


if __name__ == '__main__':
    manager.run()
