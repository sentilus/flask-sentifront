from __future__ import print_function
import math
import numpy as np
from scipy.integrate import ode
from matplotlib import pyplot as plt

class CSTR(object):

    def __init__(self, F, CA_in, V, k):
        self.F = F
        self.CA_in = CA_in
        self.V = V
        self.k = k

    def tank(self, t, y):

        CA = y[0]

        n = len(y)
        dydt = np.zeros((n, 1))
        dydt[0] = self.F/self.V*(self.CA_in - CA) - self.k*CA**2

        return dydt

    def driver(self):

        r = ode(self.tank).set_integrator('vode', method='bdf')

        t_start = 0.0
        t_final = 10.0
        delta_t = 0.1

        num_steps = math.floor((t_final - t_start)/delta_t) + 1
        CA_t_zero = 0.5
        r.set_initial_value([CA_t_zero], t_start)

        self.t = np.zeros((num_steps, 1))
        self.CA = np.zeros((num_steps, 1))
        self.t[0] = t_start
        self.CA[0] = CA_t_zero

        i = 1
        while r.successful() and i < num_steps:
            r.integrate(r.t + delta_t)
            self.t[i] = r.t
            self.CA[i] = r.y[0]
            print('Time %.2f: CA %.2f' % (r.t, r.y[0]))
            i += 1

    def plot(self):
        plt.plot(self.t, self.CA)
        plt.grid('on')
        plt.xlabel('Time [minutes]')
        plt.ylabel('Concentration')
        plt.show()

    def run(self):
        self.driver()
        self.plot()


if __name__ == '__main__':

    c = CSTR(20.1, 2.5, 100, 0.15)
    c.run()

