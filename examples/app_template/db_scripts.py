from datetime import datetime as dtdt
from sqlalchemy.engine import reflection
from sqlalchemy.sql.ddl import DropConstraint, DropTable
from sqlalchemy import MetaData, ForeignKeyConstraint, Table


def populate_users(app, db):
    with app.app_context():
        user_datastore = app.config['user_datastore']
        administrator = user_datastore.create_role(name='administrator')
        user_datastore.create_role(name='operator')
        admin = user_datastore.create_user(
            email='admin', password='tallships', name='admin',
            confirmed_at=dtdt.now())
        user_datastore.create_user(email='common', password='123456',
                                   name='common')
        user_datastore.add_role_to_user(admin, administrator)
        user_datastore.activate_user(admin)
        db.session.commit()


def drop_everything(db):
    # From http://www.sqlalchemy.org/trac/wiki/UsageRecipes/DropEverything

    conn = db.engine.connect()

    # the transaction only applies if the DB supports
    # transactional DDL, i.e. Postgresql, MS SQL Server
    trans = conn.begin()

    inspector = reflection.Inspector.from_engine(db.engine)

    # gather all data first before dropping anything.
    # some DBs lock after things have been dropped in
    # a transaction.
    metadata = MetaData()

    tbs = []
    all_fks = []

    for table_name in inspector.get_table_names():
        fks = []
        for fk in inspector.get_foreign_keys(table_name):
            if not fk['name']:
                continue
            fks.append(
                ForeignKeyConstraint((), (), name=fk['name'])
            )
        t = Table(table_name, metadata, *fks)
        tbs.append(t)
        all_fks.extend(fks)

    for fkc in all_fks:
        conn.execute(DropConstraint(fkc))

    for table in tbs:
        conn.execute(DropTable(table))

    trans.commit()