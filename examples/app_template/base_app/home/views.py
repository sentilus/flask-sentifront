from flask import Blueprint, redirect, url_for, render_template

home = Blueprint('home', __name__, template_folder='templates')


@home.route('/')
def homepage():
    return render_template('homepage.html')
