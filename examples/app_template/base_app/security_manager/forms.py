from flask.ext.security import RegisterForm, ConfirmRegisterForm
from wtforms import TextField
from wtforms.validators import Required, Optional
from wtforms.form import Form
import flask.ext.wtf as flask_wtf

class OrderedFields(Form):

    def __iter__(self):
        field_order = getattr(self, 'field_order', None)
        if field_order:
            temp_fields = []
            for name in field_order:
                if name == '*':
                    temp_fields.extend([f for f in self._unbound_fields if f[0] not in field_order])
                else:
                    temp_fields.append([f for f in self._unbound_fields if f[0] == name][0])
            self._unbound_fields = temp_fields
        return super(OrderedFields, self).__iter__()


class NameRegisterForm(OrderedFields, RegisterForm):
    name = TextField('Name', [Required()])
    field_order = ('name', '*')


class ChangeUserDetailsForm(flask_wtf.Form):
    name = TextField('Name', [Optional()])
    email = TextField('Email Address', [Optional()])