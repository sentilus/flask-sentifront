from flask import Blueprint, render_template, redirect, url_for
from flask.ext.security import current_user, login_required
from .forms import ChangeUserDetailsForm
from ..extensions import db
from .security_models import User

security_manager = Blueprint('security_manager', __name__,
                             template_folder='templates')


@login_required
@security_manager.route('/user_home')
def user_home():
    return render_template('user_home.html')


@login_required
@security_manager.route('/change_details', methods=['GET', 'POST'])
def change_details():
    user = db.session.query(User).filter(User.id == current_user.id).first()
    form = ChangeUserDetailsForm(obj=user)
    if form.validate_on_submit():
        form.populate_obj(user)
        db.session.commit()
        return redirect(url_for('.user_home'))
    return render_template('change_details.html', form=form)