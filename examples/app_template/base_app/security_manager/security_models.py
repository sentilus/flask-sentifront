from sqlalchemy import Column, String, Table, ForeignKey, Integer, \
    UniqueConstraint, \
    func, collate
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.types import VARCHAR
from sqlalchemy.sql.expression import FunctionElement
from flask_security import UserMixin, RoleMixin

from ..extensions import db, frontend
from ..models import CaseInsensitive


class BaseModel():
    id = Column(Integer, primary_key=True, autoincrement=True)


class CaseInsensitive(FunctionElement):
    """Case insensitive element that can be used as a SQL index

    Note that this can only be compiled on postgresql and sqlite databases.

    """

    __visit_name__ = 'notacolumn'
    name = 'CaseInsensitive'
    type = VARCHAR()


@compiles(CaseInsensitive, 'sqlite')
def case_insensitive_sqlite(element, compiler, **kw):
    arg1, = list(element.clauses)
    return compiler.process(collate(arg1, 'nocase'), **kw)


@compiles(CaseInsensitive, 'postgresql')
def case_insensitive_postgresql(element, compiler, **kw):
    arg1, = list(element.clauses)
    return compiler.process(func.lower(arg1), **kw)
# Permissions, Roles and Users

user_role = frontend.default_user_role(db)
# Role = frontend.default_role(db)


class ViewItem(db.Model):
    __tablename__ = 'view_items'

    id = Column(db.Integer, primary_key=True, autoincrement=True)
    name = Column(String, unique=True)

    def __repr__(self):
        return self.name


class Permission(db.Model):
    __tablename__ = 'permissions'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, unique=True)

    def __repr__(self):
        return self.name


class ViewPermission(db.Model):
    __tablename__ = 'view_permissions'
    __table_args__ = (UniqueConstraint('view_item_id', 'permission_id'),)

    id = Column(Integer, primary_key=True, autoincrement=True)
    view_item_id = Column(Integer, ForeignKey('view_items.id'))
    permission_id = Column(Integer, ForeignKey('permissions.id'))

    view_item_rel = db.relationship('ViewItem')
    permission_rel = db.relationship('Permission')

    def __repr__(self):
        return '%s on %s' % (self.permission_rel, self.view_item_rel)


role_permissions = Table(
    'role_permissions', db.metadata,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('view_permission_id', Integer, ForeignKey('view_permissions.id')),
    Column('role_id', Integer, ForeignKey('role.id')))


class Role(db.Model, RoleMixin):
    __tablename__ = 'role'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    description = db.Column(db.String)

    permissions = db.relationship('ViewPermission', secondary=role_permissions)

    def __repr__(self):
        return self.name


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    name = db.Column(db.String, nullable=False)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())

    roles = db.relationship('Role', secondary=user_role)

    @classmethod
    def get_by_username(cls, username):
        """
        :rtype : User
        """
        return cls.query.filter_by(name=username).first()

    def __repr__(self):
        return self.name