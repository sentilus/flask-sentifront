"""

.. module:: extensions
    :synopsis: Creating the extension base classes for global app access

.. moduleauthor:: Sentilus Associates

"""
import flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.mail import Mail
from flask.ext.login import LoginManager
from flask.ext.admin import Admin
from flask.ext.sentifront import Frontend
from flask_reporting import Reporting
from flask.ext.security import Security
from celery import Celery

db = SQLAlchemy()
mail = Mail()
login_manager = LoginManager()
admin = Admin()
frontend = Frontend()
reporting = Reporting()
security = Security()

'''
CELERY how-to (async emails and taskas)
Need redis server installed

Create celery worker by running the following command in /(project)
    celery worker -A manage.celery_app --loglevel=info

Add any async tasks to base_app/tasks_async.py and import as necessary
'''


class FlaskCelery(Celery):

    def __init__(self, *args, **kwargs):

        super(FlaskCelery, self).__init__(*args, **kwargs)
        self.patch_task()

        if 'app' in kwargs:
            self.init_app(kwargs['app'])

    def patch_task(self):
        TaskBase = self.Task
        _celery = self

        class ContextTask(TaskBase):
            abstract = True

            def __call__(self, *args, **kwargs):
                if flask.has_app_context():
                    return TaskBase.__call__(self, *args, **kwargs)
                else:
                    with _celery.app.app_context():
                        return TaskBase.__call__(self, *args, **kwargs)

        self.Task = ContextTask

    def init_app(self, app):
        self.app = app
        self.config_from_object(app.config)

celery_app = FlaskCelery(include='base_app.tasks_async')