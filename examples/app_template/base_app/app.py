"""
.. module:: app
    :synopsis: Initialisation of the Flask app

.. moduleauthor:: Sentilus Associates

"""

import os
from os import path
import logging
from logging.handlers import SMTPHandler

from flask import Flask
from flask.ext.security import SQLAlchemyUserDatastore
from .security_manager.views import security_manager
from .security_manager.security_models import User, Role, user_role
from .security_manager.forms import NameRegisterForm
from .home.views import home
from .extensions import *


__all__ = ['create_app']


def create_app(config=None, app_name=None):
    """Create the Flask app

    :type config: BaseConfig
    :param config:
        Configuration class for the app
    :type app_name: str
    :param app_name:
        Application name - important for registering blueprints

    :returns: An application object
    :rtype : flask.Flask

    """
    # Reverts to package name if None
    if app_name is None:
        app_name = 'base_app'

    app = Flask(app_name, template_folder='templates')
    app.config.from_object(config)

    return app


def setup_app(app):
    with app.app_context():
        configure_logging(app)
        app.register_blueprint(security_manager)
        app.register_blueprint(home)
        configure_extensions(app)

    return app


def configure_extensions(app):

    """Initiate the specified extensions on the app. """

    db.init_app(app)
    app.logger.info('Database initialised')

    admin.init_app(app)
    app.logger.info('Admin initialised')

    frontend.init_app(app, db, user=User, role=Role, user_role=user_role)
    app.logger.info('Sentilus frontend initialised')

    app.config['user_datastore'] = SQLAlchemyUserDatastore(db, User, Role)
    security.init_app(app, app.config['user_datastore'],
                      register_form=NameRegisterForm, confirm_register_form=NameRegisterForm)
    app.logger.info('Security initialised')

    mail.init_app(app)
    app.logger.info('Mail initialised')

    celery_app.config_from_object(app)
    app.logger.info('Celery initialised')


def configure_logging(app, logging_level=logging.INFO):
    """Configure file(info) and email(error) logging.

    If app.debug or app.testing:
        Skip debug and test mode - Just check standard output.

    Suppresses DEBUG messages by default.

    """
    app.logger.setLevel(logging_level)

    info_log = os.path.join(app.config['LOG_FOLDER'], 'info.log')
    info_file_handler = logging.FileHandler(info_log)
    info_file_handler.setLevel(logging.INFO)
    info_file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]'))
    app.logger.addHandler(info_file_handler)

    # Testing Configuration
    app.logger.info("testing info.")
    app.logger.warn("testing warn.")
    app.logger.error("testing error.")

