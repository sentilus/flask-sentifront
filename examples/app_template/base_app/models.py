"""
Data Models Module
------------------

Create the datamodels for the app and add them to the database using sqlalchemy

"""

from sqlalchemy import Column, String, Float, DateTime, Table, BigInteger, \
    ForeignKey, Integer, Date, Boolean, UniqueConstraint, \
    func, collate, Index, and_, event
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.orm import backref, aliased
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.types import VARCHAR
from sqlalchemy.sql.expression import FunctionElement
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from flask_security import UserMixin, RoleMixin
from flask.ext.reporting.models import ReportsMixin, AnalysisMixin, \
    SavedTablesMixin

from .extensions import db, frontend


class BaseModel():
    id = Column(Integer, primary_key=True, autoincrement=True)


class CaseInsensitive(FunctionElement):
    """Case insensitive element that can be used as a SQL index

    Note that this can only be compiled on postgresql and sqlite databases.

    """

    __visit_name__ = 'notacolumn'
    name = 'CaseInsensitive'
    type = VARCHAR()


@compiles(CaseInsensitive, 'sqlite')
def case_insensitive_sqlite(element, compiler, **kw):
    arg1, = list(element.clauses)
    return compiler.process(collate(arg1, 'nocase'), **kw)


@compiles(CaseInsensitive, 'postgresql')
def case_insensitive_postgresql(element, compiler, **kw):
    arg1, = list(element.clauses)
    return compiler.process(func.lower(arg1), **kw)

