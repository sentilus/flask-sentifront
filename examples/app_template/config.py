import os
import tempfile

basedir = os.path.abspath(os.path.dirname(__file__))

class BaseConfig():

    '''Contains the base configuration '''

    CLIENT_MODULE='Test App'

    INSTANCE_FOLDER_PATH = os.path.join(tempfile.gettempdir(), 'sentiprocess_logs')
    LOG_FOLDER = INSTANCE_FOLDER_PATH
    if not os.path.isdir(INSTANCE_FOLDER_PATH):
        os.makedirs(INSTANCE_FOLDER_PATH)

    SECRET_KEY = 'This string will be replaced with a proper key in production.'
    if 'DATABASE_URL' in os.environ:
        SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL'].replace("'", "")
    else:
        SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:Sparkl3y@localhost/demo_db'
        # SQLALCHEMY_DATABASE_URI = 'sqlite:///new_testdata.db'

    DATABASE_CONNECT_OPTIONS = {}
    DATABASE_URL = SQLALCHEMY_DATABASE_URI

    CSRF_ENABLED = True
    CSRF_SESSION_KEY = "jumpingoversmallpigsflyingonsquerylsiscool"

    DEBUG = False
    EMAIL = 'test@test.com'

    # Security
    SECURITY_REGISTERABLE = True
    SECURITY_CONFIRMABLE = True
    LOGIN_DISABLED = False
    SECURITY_MSG_UNAUTHORIZED = ['Not authorized to access resource', 'info']
    SECURITY_RECOVERABLE = True
    SECURITY_CHANGEABLE = True

    # EMAIL SETTINGS
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'projects@sentilus.co.za'
    MAIL_PASSWORD = 'Sparkl3y'
    MAIL_DEFAULT_SENDER = 'projects@sentilus.co.za'

    SECURITY_LOGIN_USER_TEMPLATE = 'sentisecurity/login_user.html'
    SECURITY_REGISTER_USER_TEMPLATE = 'register_user.html'
    SECURITY_RESET_PASSWORD_TEMPLATE = 'reset_password.html'
    SECURITY_SEND_CONFIRMATION_TEMPLATE = 'send_confirmation.html'
    SECURITY_CHANGE_PASSWORD_TEMPLATE = 'change_password.html'
    SECURITY_FORGOT_PASSWORD_TEMPLATE = 'forgot_password.html'
    SECURITY_POST_LOGOUT_VIEW = '/login'

    # Celery Settings
    # BROKER_URL = 'redis://localhost:6379'
    # RESULT_BACKEND = 'redis://localhost:6379'


class DebugConfig(BaseConfig):

    '''Identical to BaseConfig but enables debugging '''

    DEBUG = True


class TestBase(BaseConfig):

    '''Identical to BaseConfig but enables debugging '''

    SQLALCHEMY_DATABASE_URI = "postgresql://postgres:Sparkl3y@localhost/flask_testing"
    DATABASE = SQLALCHEMY_DATABASE_URI
    DATABASE_URL = SQLALCHEMY_DATABASE_URI
    INSTANCE_FOLDER_PATH = os.path.join(tempfile.gettempdir(), 'lbh_webapp_logs')
    LOG_FOLDER = INSTANCE_FOLDER_PATH
    if not os.path.isdir(INSTANCE_FOLDER_PATH):
        os.makedirs(INSTANCE_FOLDER_PATH)

    SECRET_KEY = 'This string will be replaced with a proper key in production.'

    CSRF_ENABLED = True
    CSRF_SESSION_KEY = "jumpingoversmallpigsflyingonsquerylsiscool"

    DEBUG = False
    EMAIL = 'test@test.com'
    TESTING = True
    WTF_CSRF_ENABLED = False

    SQLALCHEMY_ECHO = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = False

    DATABASE_URI = "postgresql://postgres:Sparkl3y@localhost/flask_testing"
    DATABASE = DATABASE_URI
    # LOGIN_DISABLED = True
    LIVESERVER_PORT = 1337

class TestConfig():

    '''Contains the configuration for testing '''
    INSTANCE_FOLDER_PATH = os.path.join(tempfile.gettempdir(), 'lbh_webapp_logs')
    LOG_FOLDER = INSTANCE_FOLDER_PATH
    if not os.path.isdir(INSTANCE_FOLDER_PATH):
        os.makedirs(INSTANCE_FOLDER_PATH)

    SECRET_KEY = 'This string will be replaced with a proper key in production.'

    CSRF_ENABLED = True
    CSRF_SESSION_KEY = "jumpingoversmallpigsflyingonsquerylsiscool"

    DEBUG = False
    EMAIL = 'test@test.com'
    TESTING = True
    WTF_CSRF_ENABLED = False

    SQLALCHEMY_ECHO = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = False

    DATABASE_URI = "postgresql://postgres:Sparkl3y@localhost/flask_testing"
    DATABASE = DATABASE_URI
    # LOGIN_DISABLED = True
    LIVESERVER_PORT = 1337