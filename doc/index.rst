.. flask-sentifront documentation master file, created by
sphinx-quickstart on Thu Jun 26 10:44:02 2014.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.

Welcome to flask-sentifront's documentation!
============================================

Flask-Sentifront is an in-house platform for rapidly deploying
Sentilus-flavoured apps.

Contents:

.. toctree::
:maxdepth: 2

       base



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

