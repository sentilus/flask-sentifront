import sys
import os
from copy import deepcopy
from datetime import datetime as dtdt
import unittest
import copy
import datetime

import pandas as pd
import unittest
from sqlalchemy import create_engine, Column
from sqlalchemy.orm import sessionmaker, aliased, relationship
import testing.postgresql
import datetime
import numpy as np


sys.path.append('../')

from flask_sentifront.tests.test_models import User, Country, Base, Role,user_role

from flask_sentifront.tools.collection import iExcelCollector, CollectionError, gen_collector
from flask_sentifront.tools.collection import ExcelMapper as exm
from flask_sentifront.tools.collection import type_mapper_dict
from flask_sentifront.tools.io_helper import query_to_df


class CollectUsers(iExcelCollector):

    pob = aliased(Country, name='POB')

    excel_mappers = [
        exm(User.id, None, 'ID'),
        exm(User.name, None, 'Name'),
        exm(User.email, None, 'E-Mail'),
        exm(User.join_date, None, 'Join Date', excel_format='date',
            col_width=20),
        exm(Country.name, User.country_id, 'Nationality', is_validation=True),
        exm(pob.name, User.pob_id, 'Place of Birth', is_hidden=True,
            is_validation=True),
        exm(User.active, None, 'Active'),
        exm(User.weight, None, 'Weight'),
        exm(User.last_login, None, 'Last Login', excel_format='date'),
        exm(User.lunch_time, None, 'Lunch Time'),
        exm(User.code, None, 'Code'),
        exm(User.login_count, None, 'Login Count'),
        exm(None, None, 'Weight+Login', ignore_upload=True),
        exm(Role.description,None,'Role Description', ignore_upload=True), #For Descriptions ignore upload as key is swapped on name
        exm(Country.description, None, 'Country Description',
            ignore_upload=True)
    ]


class CollectionTest(unittest.TestCase):

    def setUp(self):
        # self.postgres = testing.postgresql.Postgresql()
        # engine = create_engine(self.postgres.url())
        self.engine = create_engine(
            'postgresql://tester:Pa55M3@localhost/unit_testing')
        Base.metadata.drop_all(self.engine)
        Base.metadata.create_all(self.engine)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        self.populate_db()
        self.user_collector = CollectUsers(User, self.session)

    def populate_db(self):
        france = Country(name='France')
        peru = Country(name='Peru')
        user1 = User(name='Fanie', email='Fanie@faan.com', nationality=france)
        user2 = User(name='Sanie', email='Sanie@saan.com', nationality=peru)
        role1 = Role(name='admin')
        role2 = Role(name='pleb')
        user1.roles.append(role1)
        user1.roles.append(role2)
        self.session.add_all([user1, user2])
        self.session.commit()

    def test_create(self):
        self.user_collector.create_excel(['Name'], 'collect_test.xlsx',
                                         'test')

    def test_collect_insert(self):
        # Collection
        self.user_collector.collect('new_users.xlsx', 'Sheet1', ignore='id',
                                    dir='test_data',
                                    parse_dates=['Join Date'])
        original_df = deepcopy(self.user_collector.df)
        assert original_df.iloc[0, 0] == 'Janie'
        assert isinstance(original_df.iloc[0, 3], dtdt)

        # Swapping of foreign keys
        self.user_collector.swap_foreignkeys()
        cols = self.user_collector.df.axes[1]
        assert 'country_id' in cols and 'Nationality' not in cols
        assert 'email' in cols and 'E-Mail' not in cols
        for index, fk in enumerate(self.user_collector.df['country_id']):
            if not pd.isnull(fk):
                assert isinstance(fk, int)
                country_name = self.session.query(Country.name) \
                    .filter(Country.id == fk).first()
                if country_name is None:
                    raise AssertionError('Reverse index not found')
                else:
                    assert country_name[0] == original_df['Nationality'].iloc[
                        index]
        fe = deepcopy(self.user_collector.errors.foreign_errors)
        assert 'countries' in fe
        assert 'name' in fe['countries']
        assert 'Mars' in fe['countries']['name']
        assert 'Jupiter' in fe['POB']['name']

        # Insertion
        self.user_collector.insert(ignore='id')
        assert 'Random Column' not in self.user_collector.df.axes[1]
        janie = self.session.query(User).filter(User.name == u'Janie').first()
        if janie is None:
            raise AssertionError('New entry not inserted')
        else:
            peru_id = self.session.query(Country.id) \
                .filter(Country.name == 'Peru').one()[0]
            assert janie.pob_id == peru_id
        row_errors = deepcopy(self.user_collector.errors.row_errors)
        assert 'janie@jaan.co.za' in row_errors[3]['duplicates']['email']
        assert 'email' in row_errors[4]['missing_nullables']

    def test_collected_types(self):
        self.user_collector.collect('new_users.xlsx', 'Sheet1',
                                    dir='test_data')
        test_user = self.user_collector.df.iloc[0, :]
        assert isinstance(test_user['Lunch Time'], datetime.time)
        assert isinstance(test_user['Last Login'], datetime.datetime)
        assert isinstance(test_user['Active'], bool)
        assert isinstance(test_user['Join Date'], datetime.date)
        assert isinstance(test_user['Weight'], float)
        assert test_user['Code'] == '005'
        assert isinstance(test_user['Login Count'], int)
        assert isinstance(test_user['Weight'], float)

        self.user_collector.replace_insert(ignore='id')
        janie = self.session.query(User).filter(User.name == u'Janie').first()
        assert isinstance(janie.lunch_time, datetime.time)
        #        assert isinstance(janie.last_login, datetime.datetime)
        assert isinstance(janie.join_date, datetime.date)
        assert isinstance(janie.active, bool)
        assert isinstance(janie.login_count, int)
        assert isinstance(janie.weight, float)
        assert janie.code == '005'

    def test_excel(self):
        #Collect an insert data, see if new id's are created
        excel_formulas = {'Weight+Login':
                              {'variables': ['Weight', 'Login Count'],
                               'formula': lambda x, y: '=%s + %s' % (x, y)}}
        self.user_collector.collect('new_users.xlsx', 'Sheet1',
                                    dir='test_data',
                                    parse_dates=['Join Date', 'Last Login'])
        first_df = copy.deepcopy(self.user_collector.df)
        self.user_collector.replace_insert(ignore='id', )

        #Create excel
        self.user_collector.create_excel(None, 'test_data/new_users_write.xlsx'
                                         , 'test_sheet',
                                         excel_formulas=excel_formulas)
        self.user_collector.collect('new_users_write.xlsx', 'test_sheet',
                                    dir='test_data',
                                    parse_dates=['Join Date', 'Last Login'])

        #Apply changes and edit data that needs tp be printed
        old_df = copy.deepcopy(self.user_collector.df)
        #Add some old ID's
        old_df['ID'] = None
        old_df['Weight'] = 'some string'
        old_df['Name'] = old_df['Name'] + 'New'
        old_df['E-Mail'] = old_df['E-Mail'] + 'st'
        old_df = old_df.append(copy.deepcopy(self.user_collector.df))
        old_df['E-Mail'][old_df['E-Mail'] == 'janie@jaan.co.za'] = \
            'janie_se_super_cool_nuwe_email@jaan.co.za'

        #Write the collect and insert
        self.user_collector.create_excel(None, 'test_data/new_users_write.xlsx'
                                         , 'test_sheet', df=old_df,
                                         excel_formulas=excel_formulas)
        self.user_collector.collect('new_users_write.xlsx', 'test_sheet'
                                    , dir='test_data', parse_dates=['Join Date'
                , 'Last Login'])

        #formulas are not calculated from the beginning
        assert sum(self.user_collector.df['Weight+Login'].values) == 0

        self.user_collector.replace_insert(ignore='id')

        #Get data back from the database
        data = query_to_df(self.session.query(User.__table__))

        #Ensure integrity throughout the process
        #assert updates can be made
        assert 'janie_se_super_cool_nuwe_email@jaan.co.za' == \
               data['email'][data['name'] == 'Janie'].values[0]
        #assert all ten records exists
        assert len(data.index) == 10
        #assert dates remain stable
        assert 'janie_se_super_cool_nuwe_email@jaan.co.za' == \
               data['email'][data['name'] == 'Janie'].values[0]

        #Some type conversions
        database_date = data['join_date'][data['name'] == 'Janie'].values[0]
        dt64 = first_df['Join Date'][first_df['Name'] == 'Janie'].values[0]
        ts = (dt64 - np.datetime64('1970-01-01T00:00:00Z')) \
             / np.timedelta64(1, 's')

        assert database_date == dtdt.utcfromtimestamp(ts).date()

    def test_print_excel_empty(self):
        users=self.session.query(User).all()

        for user in users:
            user.roles[:] = []
        #remove all roles
        self.session.commit()
        self.session.query(User).delete() #.query.delete()

        self.user_collector.create_excel(None, 'test_data/new_users_write.xlsx'
                                         , 'test_sheet', df=None,)

    def test_obj_parse(self):
        import types
        from pprint import pprint
        from sqlalchemy import inspect
        test_obj = {
            'User': {
                'id': None,
                'Country': {
                    'id': None,
                    'name': 'Peru',
                    '__foreign_key__': 'country_id',
                    '__rel__': 'parent'
                },
                'name': 'Percy',
                'email': 'p@p.com',
                '__col_type__': 'orm'
            }
        }

        def str_to_class(str):
            return getattr(sys.modules[__name__], str)

        for obj_name in test_obj:
            obj = str_to_class(obj_name)
            if obj_name['id'] is None:
                obj_inst = obj()
            else:
                obj_inst = self.session.query(obj) \
                    .filter(obj.id == obj_name['id']).first()
            for c in test_obj[obj_name]:
                if isinstance(test_obj[obj_name][c], dict):
                    foreign_obj = str_to_class(c)
                    foreign_key_name = test_obj[obj_name][c]['__foreign_key__']
                    queries = []
                    for foreign_item_name in test_obj[obj_name][c]:
                        if foreign_item_name != 'id' and \
                                not foreign_item_name.startswith('__'):
                            foreign_col = getattr(foreign_obj,
                                                   foreign_item_name)
                            queries.append(
                                foreign_col ==
                                test_obj[obj_name][c][foreign_item_name])
                    result = self.session.query(foreign_obj).filter(*queries).first()
                    if result is None:
                        pass
                    else:
                        setattr(obj_inst, foreign_key_name, result.id)

                elif not c.startswith('__') and c != 'id':
                    if hasattr(obj, c):
                        setattr(obj_inst, c, test_obj[obj_name][c])

            print obj_inst.country_id
            self.session.add(obj_inst)
            self.session.commit()


    def test_auto_mappers(self):

        self.user_collector.create_excel(None, 'test_data/new_users_write.xlsx'
                                         , 'test_sheet', df=None,)
        user_collector = gen_collector(Country,self)()

        user_collector.download_foreignkeys()

    def tearDown(self):
        self.session.close()


if __name__ == '__main__':
    unittest.main()