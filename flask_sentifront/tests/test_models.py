from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (String, Integer, Column, Table, MetaData, ForeignKey,
                        Date, DateTime, Float, Boolean, Time)
from sqlalchemy.orm import relationship

test_metadata = MetaData()
Base = declarative_base(metadata=test_metadata)


user_role = Table('role_users', test_metadata,
                  Column('id', Integer, primary_key=True, autoincrement=True),
                  Column('role_id', Integer, ForeignKey('roles.id')),
                  Column('user_id', Integer, ForeignKey('users.id')))

class Country(Base):

    __tablename__ = 'countries'
    _default_name_ = 'name'
    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    description = Column(String)

class User(Base):

    __tablename__ = 'users'
    _default_name_  = 'name'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    email = Column(String, unique=True, nullable=False)
    join_date = Column(Date)
    country_id = Column(Integer, ForeignKey('countries.id'))
    pob_id = Column(Integer, ForeignKey('countries.id'))
    weight = Column(Float)
    last_login = Column(DateTime)
    lunch_time = Column(Time)
    active = Column(Boolean, default=False)
    code = Column(String)
    login_count = Column(Integer)

    roles = relationship('Role', secondary=user_role)
    nationality = relationship('Country', foreign_keys=[country_id])
    pob = relationship('Country', foreign_keys=[pob_id])


class Role(Base):

    __tablename__ = 'roles'
    _default_name_ = 'name'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, unique=True, nullable=False)
    description = Column(String)
