import sys
import abc
from os import path
import logging
from os import path
from datetime import datetime as dtdt
from io import StringIO
from flask import Markup
import numpy as np
import pandas as pd
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell, xl_col_to_name
import six
from sqlalchemy.sql import update
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from sqlalchemy import func
from sqlalchemy.types import String, Integer
from .io_helper import attribute_names, query_to_df, type_mapper
from .io_helper import attribute_names, query_to_df, type_mapper
from sqlalchemy import Column, String, Float, DateTime, Table, BigInteger, \
    Date, Time, SmallInteger, Integer, LargeBinary, Boolean
from sqlalchemy.inspection import inspect
import json
from ..form.widgets import Select2TagsWidget, Select2Widget
from wtforms import SelectMultipleField
import wtforms

log = logging.getLogger(__name__)

# from .io_helper import attribute_names, query_to_df,

def type_mapper_dict(excel_mappers):
    type_dict = {}
    for mapper in excel_mappers:
        if mapper.original_column is not None:
            col_type = mapper.original_column.type
            pandas_type = type_mapper(col_type)
            type_dict.update({mapper.excel_column: pandas_type})

    return type_dict


def mssql_server_update(orm, session, mydict):
    u = update(orm.__table__)
    uid = mydict.pop('id')
    # inst = orm(**mydict)
    u = u.values(mydict)
    u = u.where(orm.__table__.c.id == uid)
    session.execute(u)


class iExcelCollector:
    __metaclass__ = abc.ABCMeta

    # List of excel mappers
    excel_mappers = []
    filters = None
    ignore = None
    orm = None
    added_items = []
    changed_items = []


    def __init__(self, orm=None, session=None):
        """ Constructor

        :param orm: SQLAlchemy ORM
            ORM object for upload/download
        :param session:
            Scoped SQLAlchemy db session

        """
        if orm is not None:
            self.orm = orm
        if session is not None:
            self.session = session
        self.df = None
        self.filename = None
        self.errors = CollectionError()


    def replace_insert(self, **kwargs):

        """Replace foreign keys in DataFrame and insert into db """

        self.swap_foreignkeys()
        self.insert(**kwargs)

    default_handson_formats = {
        'rands': 'R0',
        'percentages': '0%',
        'number': '0',
        'number_no_dec': '0.0',
        'number_two_dec': '0.00'}

    def get_handson_settings(self, mappers=None, categories=None, filters=None, col_filters=None):
        if categories is None:
            categories = []
        columns = []
        headers = []
        used_headers = []
        search_fields = []
        max_widths = []
        selected_filters = []
        if mappers == None:
            mappers = self.excel_mappers
        i = 0

        for mapper in mappers:
            if mapper.is_hidden:
                if mapper.category is None:
                    mapper.category = ['Detail']
                else:
                    mapper.category.append('Detail')

            if mapper.category is None:
                go_on = True
            else:
                go_on = False
                for category in categories:
                    print(categories)
                    if any(category in s for s in mapper.category):
                        go_on = True

            if go_on:
                format = {'data': i}

                if mapper.is_validation:
                    val_query = self.session.query(mapper.original_column)

                    if mapper.validation_filter is not None:
                        val_query = self.add_join_filters(
                            getattr(self, mapper.validation_filter),
                            val_query)
                    data = val_query.distinct().all()
                    data = [item[0] for item in data]

                    format['type'] = 'dropdown'
                    format['source'] = data

                if mapper.original_column is None or mapper.ignore_upload:
                    # Calculated fields and ignored fields
                    format['readOnly'] = True
                if mapper.excel_format is not None:
                    if mapper.excel_format in self.default_handson_formats:
                        format['type'] = 'numeric'
                        format['format'] = self.default_handson_formats[mapper.excel_format]

                    if mapper.excel_format in ['datetime', 'date']:
                        format['type'] = 'date'
                if mapper.original_column is not None:
                    if isinstance(mapper.original_column.type, Boolean):
                        format['type'] = 'dropdown'
                        format['source'] = ['true', 'false']
                if mapper.totals is not None:
                    # get indexes
                    format['totals'] = mapper.totals

                values = self.download_foreignkeys(filters=filters, column=mapper.original_column, distinct=True)

                if values is not None:

                    if (col_filters is None or mapper.original_column is None \
                                or not mapper.save_filter):
                        col_filter = [True] * len(values)
                        if values is not None:
                            values = set(values[values.columns[1]].values)
                            search_fields.append(list([str(value) for value in values]))
                    else:
                        col_filter = []
                        if values is not None:
                            values = set(values[values.columns[1]].values)
                            search_fields.append(list([str(value) for value in values]))

                            if col_filters[mapper.excel_column] is not None:
                                # Append select options lists
                                col_type_mapper = type_mapper(mapper.original_column.type)

                                filter_vals = [col_type_mapper(val) for val in col_filters[mapper.excel_column]]
                                # invert to allow for newly added items
                                filter_vals = [val if val is not np.nan else None for val in
                                               filter_vals]  # + filter_vals_new
                                for value in values:

                                    if value in filter_vals:
                                        col_filter.append(True)
                                    else:
                                        col_filter.append(False)
                            else:
                                col_filters = [True] * len(values)
                        else:
                            col_filter = [True] * len(values)
                else:
                    col_filter = []

                if mapper.col_width is not None:
                    max_widths.append(str((mapper.col_width - 1) * 7 + 15))
                else:
                    max_widths.append(str(50))

                columns.append(format)
                selected_filters.append(col_filter)
                used_headers.append(mapper.excel_column)

            i += 1

            headers.append(mapper.excel_column)

        for format in columns:
            if 'totals' in format:
                format['totals'] = [used_headers.index(temp_name) for temp_name in format['totals']]

        return {'columns': columns, 'headers': headers,
                'used_headers': used_headers,
                'max_column_widths': max_widths,
                'search_fields': search_fields,
                'selected_filters': selected_filters}

    def apply_formulas(self, df):
        self.df = df
        if hasattr(self.orm, 'pandas_formulas'):
            for formula in self.orm.pandas_formulas:
                self.df[formula] = self.df.apply(
                    self.orm.pandas_formulas[formula]['formula'],
                    axis=1)
            return self.df
        else:
            return self.df

    def get_handson_data(self, filters, hide=True, categories=None, col_filters=None, max_rows=None, sort_by=None
    ):
        settings = self.get_handson_settings(mappers=None, filters=filters, categories=categories,
                                             col_filters=col_filters)
        df = self.download_foreignkeys(filters, col_filters=col_filters, sort_by=sort_by, max_rows=max_rows)

        if df is not None:
            df = self.apply_formulas(df)
            # ToDo find a way to not need this

            for mapper in self.excel_mappers:
                if mapper.original_column is not None:
                    if isinstance(mapper.original_column.type, (Date)) or \
                            isinstance(mapper.original_column.type, (DateTime)):
                        df[mapper.excel_column] = \
                            df[mapper.excel_column].apply(
                                lambda x: x.isoformat()
                                if not pd.isnull(x) else '')

            settings['data'] = df[settings['headers']].to_json(orient='values')#df[settings['headers']].values.tolist()
        else:
            settings['data'] = '[["No Data"]]'

        return json.dumps(settings,allow_nan=False)

    def collect_handson(self, data, headers, **kwargs):
        col_headers = str(headers)[2:-2].replace("', '", ',') + '\n'

        # convert json to csv for easy pandas parsing with configured type mappers
        data2 = StringIO(col_headers + data.replace('],[', '\n')[2:-2])
        # Use existing collect

        self.collect(data2, file_type='csv', **kwargs)
        #Drop last because it is forced to contain 1 row
        #self.df.drop(self.df.index[-1], inplace=True)


    def collect(self, filename, sheetname=None, dir=None,
                file_type='xlsx', *args, **kwargs):

        """ Collect the excel sheet into a DataFrame """

        if sheetname is None:
            sheetname = self.orm.__name__
        if isinstance(filename, str):
            assert dir is not None, """
                Must specify a relative directory
                for a string filename argument """
            filename = path.join(dir, filename)

        self.filename = filename
        self.md = type_mapper_dict(self.excel_mappers)
        if file_type is 'xlsx':
            excel_file = pd.ExcelFile(filename, engine='xlrd',
                                      converters=self.md)
            self.df = excel_file.parse(sheetname, converters=self.md,
                                       *args, **kwargs)
        elif file_type is 'json':
            self.df = pd.read_json(orient='values')
        else:
            self.df = pd.read_csv(filename, converters=self.md, engine='c'
            )

        for m in self.excel_mappers:
            if m.original_column is None:
                continue
            if isinstance(m.original_column.type, Integer):
                ints = [int(v) if pd.notnull(v) else None
                        for v in self.df[m.excel_column]]
                self.df[m.excel_column] = pd.Series(ints, dtype=object)
        return self


    def insert(self, ignore=None, check_multiple=None):
        """Insert pandas DataFrame into db"""

        if ignore is None:
            if self.ignore is not None:
                ignore = self.ignore
            else:
                ignore = list()

        self.orm_clean(self.orm)
        for index, row in self.df.iterrows():
            row_dict = dict(**row)
            mydict = {k: v for k, v in six.iteritems(row_dict) if
                      not pd.isnull(v)}

            # Go strate to updating
            is_updated = False
            if 'id' in ignore:
                if 'id' in row_dict:
                    if isinstance(row_dict['id'], (int, float)) \
                            and not pd.isnull(row_dict['id']) \
                            and row_dict['id'] != 0:
                        try:
                            # id = int(row.id)
                            inst = self.orm(**mydict)
                            #mssql explicitly requires an update
                            # mssql_server_update(self.orm, self.session,
                            #                     mydict)
                            self.session.merge(inst)

                            self.session.commit()
                            self.changed_items.append(inst)
                        except Exception as e:
                            self.errors.add_duplicate(index,
                                                      'Unknown Error '
                                                      'Check formats'
                                                      ' Unable to update ' +
                                                      str(
                                                          row), '')
                            self.session.rollback()
                            log.exception("Update record "
                                          "error: {0}".format(str(e)))
                        continue
                    else:
                        if 'id' in mydict:
                            bad_id = mydict.pop('id')
                            self.errors.add_missing_nullable(index,
                                                             'Bad ID ' + str(
                                                                 bad_id))

            complete = self.check_nullables(row, index, ignore)
            is_multiple = False
            if complete and not is_updated:

                unique = self.check_uniques(row, index, ignore)
                if check_multiple is not None:
                    multiple = self.session.query(self.orm)
                    multiple_filters = check_multiple(row)
                    for fil in multiple_filters:
                        multiple = multiple.filter(fil)
                    try:
                        mull_item = multiple.first()
                        if mull_item is not None:
                            is_multiple = True
                            self.errors. \
                                add_duplicate(
                                index, 'multiple columns are violating a '
                                       'contstraint are you trying to '
                                       'upload already uploaded data, try '
                                       'download the latest and then apply '
                                       'the changes', str(row))

                    except Exception as e:
                        is_multiple == False

                if unique and not is_multiple:

                    try:
                        inst = self.orm(**mydict)
                        self.session.add(inst)
                        self.session.commit()
                        self.added_items.append(inst)
                    except Exception as e:
                        self.errors.add_duplicate(index,
                                                  'Unknown Error Check formats'
                                                  ' Unable to insert'
                                                  + str(mydict) + str(e), '')
                        self.session.rollback()
                        log.exception("Add record error: {0}".format(str(e)))


    def clean_errors(self, foreign_errors):

        """ Clean up empty error categories """

        valid_errors = {c: e for c, e in foreign_errors.iteritems() if len(e)}

        return valid_errors

    def check_nullables(self, row, line_number, ignore=None):
        """ Check that all required columns are present

        :param row: pd.Series
            Row to be inserted
        :param line_number: int
            Line number of the row, for error reporting
        :param ignore: list
            list of column names not to be checked eg: ignore=['id']
        :return: Bool
            Boolean value for if all required fields are present

        """

        if ignore is None:
            ignore = list()

        complete = True
        for ii in row.index:
            if str(ii) not in ignore:
                if not self.orm.__table__.c[str(ii)].nullable and pd.isnull(
                        row.loc[ii]):
                    complete = False
                    self.errors.add_missing_nullable(line_number, ii)

        return complete

    def check_uniques(self, row, line_number, ignore=None):
        """ Check the unique constraints for all columns

        :param row: pd.Series
            Row to be inserted
        :param line_number: int
            Line number of the row, for error reporting
        :return:
            Boolean value for if all unique constraints are adhered to

        """
        if ignore is None:
            ignore = list()

        unique = True
        for ii in row.index:
            if str(ii) in ignore:
                continue
            value = row.loc[ii]
            column = self.orm.__table__.c[str(ii)]
            if column.unique and pd.notnull(value):
                result = self.session.query(self.orm).filter(
                    column == value).first()
                if result:
                    unique = False
                    self.errors.add_duplicate(line_number, ii, value)

        return unique

    def fmt_columns(self):

        """ Rename DataFrame columns to ORM column names """

        name_fmt = {n: n.lower().replace(' ', '_') for n in self.df.axes[1]}
        self.df.rename(columns=name_fmt, inplace=True)

    def id_sub(self, orig_column, orm, orm_attr, new_column=None, replace=True,
               required=False, filter_joins=None):
        """ Substitute column in DataFrame for its ID in its SQL Table

        :param orig_column: Original column name
        :param orm: ORM for the column
        :param orm_attr: Column name in the ORM
        :param new_column: New column name
        :param replace: Whether to replace the name of the column or append
        a new
            column
        :param required: Drop the row if the id is not found in the table

        :return: DataFrame
        :return: Errors

        """
        new_series = []
        for item in self.df[orig_column]:

            id = orm.id_from_key(orm_attr, item, joins_filters=None)
            # session.query(orm).filter(orm.__table__.c[orm_attr]==item)
            # Check if item is actually in the table
            if not pd.isnull(item) and id is None:
                if self.errors is not None:
                    self.errors.add_foreign_error(orig_column, item)
            new_series.append(id)
        new_series = pd.Series(new_series)
        if required:
            valid = list(new_series.notnull())
            self.df = self.df.iloc[valid, :]
            new_series = new_series.iloc[valid]
        if new_column is not None:
            if replace:
                self.df[orig_column] = new_series
                self.df.rename(columns={orig_column: new_column}, inplace=True)
            else:
                self.df[new_column] = new_series
        else:
            self.df[new_column] = new_series

    def swap_foreignkeys(self):

        """Map the DataFrame columns to foreign ID references """

        series_dict = {}
        for mapper in self.excel_mappers:
            if mapper.ignore_upload:
                continue

            df_column = self.df[mapper.excel_column]
            if mapper.reference_column is not None:
                foreign_id_list = []
                val_query = self.session.query(mapper.original_column
                                               .table.c.id, mapper.original_column)

                if mapper.validation_filter is not None:
                    # Validation filter is method

                    val_query = self.add_join_filters(
                        getattr(self, mapper.validation_filter),
                        val_query)
                    # Precache foreign_key value pairs

                values = val_query.all()
                if isinstance(mapper.original_column.type, String):
                    id_values = {obj[1].lower(): obj[0] for obj in values}
                else:
                    id_values = {obj[1]: obj[0] for obj in values}

                id_values = {col: id_values[col] for col in id_values if id_values[col] is not None}
                for item in df_column:
                    id = None
                    if pd.isnull(item):
                        id = None
                    elif isinstance(mapper.original_column.type, String):
                        if item.lower() in id_values:
                            id = id_values[item.lower()]
                        else:
                            self.errors.add_foreign_error(
                                str(mapper.original_column.table.name),
                                mapper.original_column.name, item)
                    else:
                        if item in id_values:
                            id = id_values[item]
                        else:
                            id = None
                            if pd.notnull(item):
                                self.errors.add_foreign_error(
                                    str(mapper.original_column.table.name),
                                    mapper.original_column.name, item)

                    foreign_id_list.append(id)
                self.df.rename(
                    columns={
                        mapper.excel_column: mapper.reference_column.name},
                    inplace=True)
                self.df.drop(mapper.reference_column.name, axis=1,
                             inplace=True)

                def convert_int(item):
                    if pd.notnull(item):
                        return int(item)

                key_series = pd.Series(foreign_id_list).apply(convert_int,
                                                              convert_dtype=False)
                self.df[
                    mapper.reference_column.name] = key_series.values
                # [convert_int(foreign_id) for foreign_id in foreign_id_list]
                # key_series
            else:
                self.df.rename(
                    columns={mapper.excel_column: mapper.original_column.name},
                    inplace=True)
                self.df[mapper.original_column.name] = df_column

    def download_foreignkeys(self, filters=None, date=None, column=None, distinct=False, col_filters=None,
                             max_rows=None, sort_by=None):
        """ Return a DataFrame from a set of filters, mapped to foreign keys

        :param filters: list
            List of SQLAlchemy filter operations
        :param foreign_filters: list
            List of SQLAlchemy equality filter operations if
            joins need to be explicity set
        :return: pd.DataFrame
            Generated DataFram

        >>> filters = [Users.id=='14', Users.registration_date<='2014-01-01']
        >>> df = UserCollector.download_foreignkeys(filters)

        """

        self.filters = filters

        query_tables = []
        foreign_tables = []
        foreign_filters = []
        inner_join = []
        column_filters = []

        sort_by_mapper = None

        for mapper in self.excel_mappers:
            if mapper.original_column is None and \
                            mapper.reference_column is None:
                continue
            if mapper.reference_column is not None:
                original_table = mapper.original_column.table
            query_tables.append(
                mapper.original_column.label(mapper.excel_column))

            if mapper.excel_column == sort_by and mapper.original_column is not None:
                sort_by_mapper = mapper

            if col_filters is not None:
                if mapper.excel_column in col_filters:
                    # TODO need to do some type conversions
                    if not isinstance(mapper.original_column.type, Float):

                        if col_filters[mapper.excel_column] is not None:
                            col_type_mapper = type_mapper(mapper.original_column.type)

                            filter_vals = [col_type_mapper(val) for val in col_filters[mapper.excel_column]]
                            filter_vals = [val if val is not np.nan else None for val in filter_vals]

                            column_filters.append(mapper.original_column.in_(filter_vals))

            if mapper.reference_column is not None:
                foreign_tables.append(original_table)
                if mapper.join_filter is None:
                    foreign_filters.append(
                        mapper.reference_column == original_table.c.id)
                else:
                    foreign_filters.append(mapper.join_filter)

                inner_join.append(mapper.inner_join)

        if column is None:
            q = self.session.query(*query_tables)
        else:
            q = self.session.query(query_tables[0], column)

        if col_filters is not None:
            q = q.filter(*column_filters)
        if filters is not None:
            q = q.filter(*filters)
        for item in range(len(foreign_filters)):
            if inner_join[item]:
                q = q.join(foreign_tables[item], foreign_filters[item])
            else:
                q = q.outerjoin(foreign_tables[item], foreign_filters[item])

        if sort_by_mapper is not None:
            q = q.order_by(sort_by_mapper.original_column)

        if max_rows is not None:
            q = q.limit(max_rows)

        if distinct:
            q = q.distinct()

        return query_to_df(q)

    def _get_errors(self):

        """ Return the collector error object """

        return self.errors

    def orm_clean(self, orm):

        """ Remove columns from DataFrame that are not in the ORM """

        table_names = attribute_names(orm)
        for col in self.df.axes[1]:
            if col not in table_names:
                self.df.drop(col, axis=1, inplace=True)

    def add_join_filters(self, joins_filters, val_query):

        return joins_filters(val_query)

    default_excel_formats = {
        'bold': {'bold': True},
        'rands': {'align': 'right', 'num_format': 'R#,##0'},
        'percentages': {'align': 'right', 'num_format': '0%'},
        'number': {'align': 'right', 'num_format': '#0'},
        'number_no_dec': {'align': 'right', 'num_format': '0.0'},
        'number_two_dec': {'align': 'right', 'num_format': '0.00'},
        'number_red': {'align': 'right', 'bg_color': '#999999', 'num_format': '#0'},
        'rands_bold': {'align': 'right', 'bold': True, 'num_format': 'R#,##0'},
        'percentages_bold': {'align': 'right', 'bold': True,
                             'num_format': '0%'},
        'number_bold': {'align': 'right', 'bold': True, 'num_format': '#0'},
        'number_no_dec_bold': {'align': 'right', 'bold': True,
                               'num_format': '#0'},
        'number_two_dec_bold': {'align': 'right', 'bold': True,
                                'num_format': '0.00'},
        'number_red_bold': {'align': 'right', 'bold': True,
                            'bg_color': '#999999', 'num_format': '#0'},
        'text': {'align': 'center_across'},
        'date': {'align': 'center_across', 'num_format': 'd mmm yyyy'},
        'datetime': {'align': 'center_across', 'num_format': 'd mmm yyyy'},
        'head_default': {'bold': True, 'bg_color': '#6495ED',
                         'text_wrap': True, 'border': 1, 'valign': 'vcenter',
                         'locked': 1},
        'head_small': {'bold': True, 'bg_color': '#6495ED',
                       'valign': 'vcenter',
                       'text_wrap': True, 'border': 1, 'font_size': 9,
                       'locked': 1},
        'head_small_red': {'bold': True, 'bg_color': '#6495ED',
                           'text_wrap': True, 'border': 1, 'font_size': 9,
                           'valign': 'vcenter', 'font_color': '#FFCC00',
                           'locked': 1},
        'head_red': {'bold': True, 'bg_color': '#6495ED', 'text_wrap': True,
                     'border': 1,
                     'font_color': '#FFCC00', 'valign': 'vcenter',
                     'locked': 1},
    }

    def create_excel(self, excel_columns, file, work_sheet_name, df=None,
                     excel_formulas=None, filters=None, max_len=900,
                     excel_formats=None, head=1, add_totals=False,
                     post_edit=None, mappers=None, hide=False):
        """
        Creates formatted excel with drop downs and formulas and coloured
        headings

        :rtype : object
        :param excel_columns: All ordered columns in final excel sheet
        :param file: Name of file or xlsxwriter work_book (To be replaced by
        stringIO)
        :param work_sheet_name: (Name of sheet to write to)
        :param df: (Optional dataframe to be inserted )
        :param excel_formula: Any excel formula specified like:
        excel_formulas = {'Control Budget':{'variables':['Baseline Budget',
        'Pending VOs'],
                        'formula':lambda x,y:('=%s + %s') % (x,y)}}
        :param filter: Additional argument if df is not provided and a user
        wants to pass
        """

        if mappers is None:
            mappers = self.excel_mappers
        if excel_columns is None:
            excel_columns = [item.excel_column for item in mappers]

        # TODO allow formats and datetime objects

        if excel_formats is None:
            excel_formats = self.default_excel_formats

        if df is None:
            excel_df = self.download_foreignkeys(filters=filters)
        else:
            excel_df = df

        if excel_df is not None:

            for mapper in mappers:
                if mapper.original_column is not None:
                    if isinstance(mapper.original_column.type, (Date)) or \
                            isinstance(mapper.original_column.type, (DateTime)):
                        excel_df[mapper.excel_column] = \
                            excel_df[mapper.excel_column].apply(
                                lambda x: dtdt.strftime(x, '%d %b %y')
                                if not pd.isnull(x) else '')

        # Todo map file to stringIO
        #output = BytesIO()
        #  http://xlsxwriter.readthedocs.org/en/latest/example_http_server
        # .html#ex-http-server
        #work_book = xlsxwriter.Workbook(output,{'in_memory': True})

        if excel_formulas is None:
            excel_formulas = dict()  # set as empty dict

        if isinstance(file, str):
            work_book = xlsxwriter.Workbook(file, {'in_memory': True})
        else:
            work_book = file

        format = work_book.add_format()
        cell_formats = dict()
        for item in excel_formats:
            cell_formats[item] = work_book.add_format(excel_formats[item])

        heading = work_book.add_format({'bold': True, 'bg_color':
            '#6495ED', 'text_wrap': True, 'border': 1})
        worksheet = work_book.add_worksheet(work_sheet_name)
        #work_book.calc_mode('A')
        #set up headings
        # if excel_df is not None:
        #     worksheet.add_table(0,0,len(excel_df.index),len(excel_columns)-1,
        #                         {'autofilter':True,'header_row': True,
        #                           'name':work_sheet_name+'_table'})
        for j in range(len(excel_columns)):
            ex_args = [args for args in mappers
                       if args.excel_column == excel_columns[j]]
            if len(ex_args) > 0:
                ex_args = ex_args[0]
                if ex_args.header_format is not None:
                    worksheet.write(0, j, excel_columns[j],
                                    cell_formats[ex_args.header_format])

                else:

                    worksheet.write(0, j, excel_columns[j],
                                    cell_formats['head_default'])

            else:
                worksheet.write(0, j, excel_columns[j],
                                cell_formats['head_default'])


        #Set number of column headers

        #Set heading Height
        worksheet.set_row(0, 70)
        if excel_df is not None:
            max_len = len(excel_df.index) + max_len

        #Set up validation
        validation_location = dict()
        validation_sheet = work_book.add_worksheet(work_sheet_name
                                                   + '_validation')
        for name in excel_columns:
            ex_args = [args for args in mappers
                       if args.excel_column == name]
            col_index = excel_columns.index(name)
            if len(ex_args) > 0:
                ex_args = ex_args[0]

                if ex_args.is_validation or ex_args.validation_link is not \
                        None:

                    val_query = self.session.query(ex_args.original_column)

                    if ex_args.validation_filter is not None:
                        val_query = self.add_join_filters(
                            getattr(self, ex_args.validation_filter),
                            val_query)

                    if ex_args.validation_link is not None:
                        ex_args_link = [args for args in mappers
                                        if
                                        args.excel_column == ex_args
                                            .validation_link]
                        val_query = val_query.order_by(
                            ex_args_link[0].original_column)
                    else:
                        val_query = val_query.order_by(ex_args.original_column)

                    data = val_query.all()

                    data = [tup[0] for tup in data]
                    # if len(str(data))>254:
                    #TODO max length specifier
                    validation_sheet.write_column(0,
                                                  col_index,
                                                  data)

                    start = xl_rowcol_to_cell(0, col_index,
                                              col_abs=True, row_abs=True)

                    stop = xl_rowcol_to_cell(len(data),
                                             col_index,
                                             col_abs=True, row_abs=True)

                    validation_location[name] = work_sheet_name + \
                                                '_validation' + '!' + \
                                                start + ':' + stop
                    if ex_args.is_validation:
                        worksheet.data_validation(1, col_index,
                                                  max_len,
                                                  col_index,
                                                  {'validate': 'list',
                                                   'source':
                                                       validation_location[
                                                           name]})
                if ex_args.validation_list is not None:
                    worksheet.data_validation(1, col_index,
                                              max_len,
                                              col_index,
                                              {'validate': 'list',
                                               'source':
                                                   ex_args.validation_list})



        #Print the actual data Only if their is data to print
        if excel_df is not None:
            tot_len = len(excel_df.axes[0])

            #Snag 1 printing needs to be done row by row #Write column does
            # not works
            for i in range(tot_len):

                for j in range(len(excel_columns)):
                    ex_args = [args for args in mappers if
                               args.excel_column == excel_columns[j]]
                    if len(ex_args) > 0:
                        ex_args = ex_args[0]
                    else:
                        ex_args = None

                    if excel_columns[j] in excel_df.columns.values:
                        if not pd.isnull(excel_df[excel_columns[j]].values[i]):
                            #Check and add formatting

                            if ex_args is not None:
                                if ex_args.excel_format is not None:
                                    worksheet.write(i + head, j,
                                                    (excel_df[excel_columns[
                                                        j]].values[i]),
                                                    cell_formats[
                                                        ex_args.excel_format])
                                else:
                                    worksheet.write(i + head, j,
                                                    (excel_df[excel_columns[
                                                        j]].values[i]))
                            #write actual data
                            else:
                                worksheet.write(i + head, j,
                                                (excel_df[
                                                     excel_columns[j]].values[
                                                     i]))

                        else:
                            #Write just formatting
                            if ex_args is not None:
                                if ex_args.excel_format is not None:
                                    worksheet.write(i + head, j, '',
                                                    cell_formats[
                                                        ex_args.excel_format])
                    #Add any excel rules
                    if excel_columns[j] in excel_formulas:
                        args = list()
                        for arg in excel_formulas[excel_columns[j]][
                            'variables']:
                            args.append(
                                xl_rowcol_to_cell(i + head,
                                                  excel_columns.index(arg)))
                        if ex_args is not None:

                            if ex_args.excel_format is not None:
                                worksheet.write_formula(i + head, j,
                                                        excel_formulas[
                                                            excel_columns[j]][
                                                            'formula'](*args)
                                                        , cell_formats[
                                                            ex_args.excel_format])
                            else:
                                worksheet.write_formula(i + head, j,
                                                        excel_formulas[
                                                            excel_columns[j]][
                                                            'formula'](*args)
                                )
                        else:
                            worksheet.write_formula(i + head, j,
                                                    excel_formulas[
                                                        excel_columns[j]][
                                                        'formula'](*args)
                            )


                    #Print the index match formula
                    if ex_args is not None:

                        if ex_args.validation_link is not None:
                            worksheet.write_formula(
                                i + head, j, '=INDEX(' + \
                                validation_location[
                                    excel_columns[
                                        j]]
                                + ',MATCH(' + \
                                xl_rowcol_to_cell(
                                    i + head,
                                    excel_columns.index(
                                        ex_args.validation_link))
                                + ',' + \
                                validation_location[
                                    ex_args
                                .validation_link] + ',0))'
                            )

        for name in excel_columns:
            ex_args = [args for args in mappers
                       if args.excel_column == name]
            col_index = excel_columns.index(name)
            if len(ex_args) > 0:
                ex_args = ex_args[0]
                worksheet.set_column(col_index, col_index,
                                     width=ex_args.col_width,
                                     options={'hidden': ex_args.is_hidden})
        validation_sheet.hide()
        #Set-up nice filters

        worksheet.autofilter('A1:' +
                             xl_rowcol_to_cell(0, len(excel_columns) - 1))

        if excel_df is not None:
            if post_edit is not None:
                worksheet = post_edit(worksheet,
                                      {'tot_len': tot_len, 'head': head,
                                       'excel_columns': excel_columns,
                                       'mappers': mappers,
                                       'cell_formats': cell_formats})

        if hide:
            worksheet.hide()
        #Save and close
        if isinstance(file, str):
            work_book.close()
            return file
        else:  #Pass to next collector to add more methods
            return work_book


class ExcelMapper(object):
    def __init__(self, original_column, reference_column, excel_column,
                 is_validation=False, col_width=None, validation_link=None,
                 pandas_formula=None, excel_formula=None,
                 excel_format=None, validation_filter=None,
                 ignore_upload=False, join_filter=None,
                 inner_join=False, header_format=None, is_hidden=False,
                 many2many=None, is_locked=None, totals=None, validation_list=None, category=None, save_filter=False):
        """ Constructor

        :param original_column: SQLAlchemy Column
            The Column from the Table referenced in the excel sheet
        :param reference_column: SQLAlchemy Column
            The reference ID column in this object's ORM
        :param excel_column: str
            Name of the column as appears in the excel sheet
        :param is_validation: bool
            If the column is needs to have validation added
        :param validation link: str
            The name of the column
        :param pandas_formula: lambda
            A lamda expression that takes a row as a dictionary
            and a calculates the column
        :param excel_formula: dict()
            A dictionary containing a list of arguments and a lamba expression
            that takes excel reference and converts it to the string
            representation of a formula
        :param validation_filter: func
            A method on the collector that returns a list of filters for
            the validation
        :param col_width: int
            column width in pixels
        param is_locked: bool
            Determine if that row is locked

        """

        self.original_column = original_column
        self.reference_column = reference_column
        self.excel_column = excel_column
        self.is_validation = is_validation
        self.col_width = col_width
        self.validation_link = validation_link
        self.pandas_formula = pandas_formula
        self.excel_formula = excel_formula
        self.validation_filter = validation_filter
        self.excel_format = excel_format
        self.join_filter = join_filter
        self.ignore_upload = ignore_upload
        self.inner_join = inner_join
        self.header_format = header_format
        self.is_hidden = is_hidden
        self.many2many = many2many
        self.is_locked = is_locked
        self.validation_list = validation_list
        self.category = category
        self.totals = totals
        self.save_filter = save_filter


class CollectionError(object):
    """ Collector Errors used for reporting in upload_errors.html """

    def __init__(self):
        self.row_errors = {}
        self.foreign_errors = {}

    def add_duplicate(self, line_number, column, value):

        """ Add an error entry for duplicate entries """

        if not line_number in self.row_errors:
            self.row_errors[line_number] = {}
        if not 'duplicates' in self.row_errors[line_number]:
            self.row_errors[line_number]['duplicates'] = {}
            self.row_errors[line_number]['duplicates'][column] = value

    def add_missing_nullable(self, line_number, column):

        """ Add a missing required value to the errors """

        if not line_number in self.row_errors:
            self.row_errors[line_number] = {}
        if not 'missing_nullables' in self.row_errors[line_number]:
            self.row_errors[line_number]['missing_nullables'] = []
        self.row_errors[line_number]['missing_nullables'].append(column)

    def add_foreign_error(self, foreign_table, foreign_column, value):

        """ Add a foreign key error """

        if not foreign_table in self.foreign_errors:
            self.foreign_errors[foreign_table] = {}
        if not foreign_column in self.foreign_errors[foreign_table]:
            self.foreign_errors[foreign_table][foreign_column] = list()

        if value not in self.foreign_errors[foreign_table][foreign_column]:
            self.foreign_errors[foreign_table][foreign_column].append(value)

    def update_foreign_errors(self, foreign_error_dict):
        for key, value in foreign_error_dict.iteritems():
            self.add_foreign_error(key, value)

    def count_row_errors(self):
        return len(self.row_errors.keys())

    def count_foreign_errors(self):
        return len(self.foreign_errors.keys())

    def __len__(self):
        return self.count_row_errors() + self.count_foreign_errors()


def id_sub(df, orig_column, orm, orm_attr, new_column=None, replace=True,
           required=False, errors=None):
    """ Substitute column in DataFrame for its ID in its SQL Table

    :param df: DataFrame for which to substitute ID
    :param orig_column: Original column name
    :param orm: ORM for the column
    :param orm_attr: Column name in the ORM
    :param new_column: New column name
    :param replace: Whether to replace the name of the column or append a new
        column
    :param required: Drop the row if the id is not found in the table
    :param errors: CollectError object

    :return: DataFrame
    :return: Errors

    """
    # new_series = pd.Series(
    # [orm.id_from_key(orm_attr, c) for c in df[orig_column]])
    new_series = []
    for item in df[orig_column]:
        id = orm.id_from_key(orm_attr, item)
        # Check if item is actually in the table
        if not pd.isnull(item) and id is None:
            if errors is not None:
                errors.add_foreign_error(orig_column, item)
        new_series.append(id)
    new_series = pd.Series(new_series)
    if required:
        valid = list(new_series.notnull())
        df = df.iloc[valid, :]
        new_series = new_series.iloc[valid]
    if new_column is not None:
        if replace:
            df[orig_column] = new_series
            df.rename(columns={orig_column: new_column}, inplace=True)
        else:
            df[new_column] = new_series
    else:
        df[new_column] = new_series

    return df


def gen_collector(mod, db):
    """ Create a default excel mapper for a ORM object

    :param mod: A SQLAlchemy ORM object
    :param db: A SQLAlchemy database object

    :return: iExcelCollector
    """

    excel_mappers = []
    list_columns = dict()
    ignore = None
    for col_name in mod.__mapper__.columns.keys():
        list_columns[col_name] = mod.__mapper__.columns[col_name]
        col = mod.__mapper__.columns[col_name]
        if len(col.foreign_keys) == 0:
            mapper = ExcelMapper(col, None, col.key)

            excel_mappers.append(mapper)
            if col_name == 'id':
                ignore = ['id']

    mod_relations = inspect(mod).relationships
    relation_classes = dict()
    for relation in mod_relations:
        # TODO check for 1 to mana
        # if relation.primaryjoin.right

        if relation.direction.name in ['ONETOMANY', 'MANYTOMANY']:
            continue
        relation_class = relation.mapper.class_
        if hasattr(relation_class, '_default_name_'):
            name = relation_class.__tablename__ + ' ' + relation_class._default_name_
            relation_classes[relation_class.__tablename__] = relation_class

            mapper = ExcelMapper(getattr(relation_class,
                                         relation_class._default_name_),
                                 relation.primaryjoin.right,
                                 name,
                                 is_validation=True)

        excel_mappers.append(mapper)

    for mapper in excel_mappers:
        mapper.excel_column = mapper.excel_column.replace('_', ' ').title()

    if db is not None:
        session = db.session

    return type(mod.__name__ + 'Collector', (iExcelCollector,),
                dict(excel_mappers=excel_mappers, orm=mod, session=session,
                     ignore=ignore))