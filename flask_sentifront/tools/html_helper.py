from bs4 import NavigableString, Tag


def clone_element(el):
    """ Clone a BeautifulSoup element

    :param el: BeautifulSoup element
    :return: Cloned copy of element

    """
    if isinstance(el, NavigableString):
        return type(el)(el)

    copy = Tag(None, el.builder, el.name, el.namespace, el.nsprefix)
    copy.attrs = dict(el.attrs)
    for attr in ('can_be_empty_element', 'hidden'):
        setattr(copy, attr, getattr(el, attr))
    for child in el.contents:
        copy.append(clone_element(child))
    return copy