from datetime import datetime as dtdt

class AnalysesTimer(object):

    def __init__(self):
        self.results = {}
        self.start_time = None
        self.current_description = None

    def start_new(self, description):
        assert self.start_time is None, 'Timer already started'
        self.start_time = dtdt.now()
        self.current_description = description
        self.results[description] = {}
        self.results[description]['description'] = self.current_description
        self.results[description]['start_time'] = self.start_time
        self.results[description]['end_time'] = None

    def end(self, record_number=0):
        self.end_time = dtdt.now()
        assert self.start_new is not None, 'Timer not started'
        self.results[self.current_description]['records'] = record_number
        self.results[self.current_description]['time_taken'] = (self.end_time - self.start_time).total_seconds()
        self.start_time = None
        self.end_time = None
        self.current_description = None
