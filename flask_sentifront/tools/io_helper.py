import copy
import sys
import pandas as pd
import sqlalchemy
from sqlalchemy import Column, String, Float, DateTime, Table, BigInteger,\
    Date, Time, SmallInteger, Integer, LargeBinary, Boolean
from sqlalchemy.orm import class_mapper, ColumnProperty
import numpy as np
from datetime import datetime, date, time ,timedelta
from dateutil.parser import parse
from six import string_types
from flask import flash, g
from babel.numbers import format_currency
from decimal import Decimal


def safe_dtypes(x, dtype, flag=False):
    if pd.isnull(x) or x is None or x == '' or x == 'null':
        return np.nan
    else:
        try:
            val = dtype(x)
            return val
        except Exception:
            #Through exception to user via flash or to stdout
            try:
                flash(str(x) + ' ' +  str(dtype) + ' Check Formatting','warning')
            except Exception:
                print(str(x) + ' ' + str(dtype) + ' Check Formatting')

            return np.nan

def safe_bool(x):
    if pd.isnull(x) or x is None or x == '' or x == 'null':
        return np.nan
    if isinstance(x, string_types):
        if 'false' in x.lower().strip():
            return False
        if 'true' in x.lower().strip():
            return True
        if 'yes' in x.lower().strip():
            return true
        if 'no' in x.lower().strip():
            return False

    return bool(x)

def safe_str(x):
    if pd.isnull(x) or x is None or x == '' or x == 'null':
        return ''
    else:
        return str(x)

def safe_bool(x):
    if pd.isnull(x) or x is None or x == '' or x == 'null':
        return np.nan
    if isinstance(x, string_types):
        if 'false' in x.lower().strip():
            return False
        if 'true' in x.lower().strip():
            return True
        if 'yes' in x.lower().strip():
            return true
        if 'no' in x.lower().strip():
            return False

    return bool(x)

def safe_dates(x):
    if pd.isnull(x) or x is None or x == '' or x == 'null':
        return np.nan
    if isinstance(x, float):
        x = int(x)
    if isinstance(x, string_types) or isinstance(x, int):
        try:
            x = parse(x)
            return x
        except Exception:
            #Through exception to user via flash or to stdout
            try:
                flash(str(x) + ' Check Date Formatting','warning')
            except Exception:
                print(str(x) + ' Check Date Formatting')

            return np.nan
    if (isinstance(x, datetime) or isinstance(x, np.datetime64)
            or isinstance(x, time)):
        return x
    return np.nan

def safe_time(x):
    if pd.isnull(x) or x is None or x == '' or x == 'null':
        return np.nan
    if isinstance(x, int):
        return time(second=x)
    if isinstance(x, float):
        value = timedelta(seconds=int(x*24*3600))
        return (datetime.min + value).time()
    if isinstance(x, time):
        return x
    return np.nan


def type_mapper(col_type):
    if isinstance(col_type, BigInteger):
        return lambda x: safe_dtypes(x, np.int64)
    elif isinstance(col_type, Boolean):
        return lambda x: safe_bool(x)
    elif isinstance(col_type, Date):
        return lambda x: safe_dates(x)
    elif isinstance(col_type, Float):
        return lambda x: safe_dtypes(x, np.float64)
    elif isinstance(col_type, DateTime):
        return lambda x: safe_dates(x)
    elif isinstance(col_type, Integer):
        return lambda x: safe_dtypes(x, int)
    elif isinstance(col_type, LargeBinary):
        return lambda x: safe_dtypes(x, np.bool)
    elif isinstance(col_type, SmallInteger):
        return lambda x: safe_dtypes(x, np.int64)
    elif isinstance(col_type, String):
        return lambda x: safe_str(x)
    elif isinstance(col_type, Time):
        return lambda x: safe_time(x)
    else:
        return None


def read_engine(session, sqlmodel, index=None, drop=None, *filterargs):
    """Returns a DataFrame from a table using the ORM.

    :param session: Database engine session
    :param sqlmodel: The ORM
    :param index: Index column of the returned DataFrame
    :param drop: List of columns to remove from DataFrame
    :param filterargs: List of filter arguments to apply to the query

    :rtype :pd.Dataframe
    :returns: DataFrame representing the SQL query

    """
    if filterargs is not None:
        results = session.query(sqlmodel).filter(*filterargs)
    else:
        results = session.query(sqlmodel)

    if drop is None:
        drop = []

    return sqlquery_to_dataframe(results, sqlmodel, index, drop)


def populate_engine(session, df, sqlmodel):
    """Populate SQLAlchemy database with a DataFrame. """

    for i in range(len(df.axes[0])):
        record_dict = df.iloc[i, :].to_dict()
        record = sqlmodel(**record_dict)

        session.merge(record)

    session.commit()


def table_to_df(table, session):
    """ Convert an entire table to a DataFrame

    :param table: SQLAlchemy table object
    :param session: SQLAlchemy database session
    :return: DataFrame of the table, None if empty query

    """
    table_select = table.select(use_labels=True)
    query = session.execute(table_select)
    df = pd.DataFrame(query.fetchall())
    if df.shape[0] < 1:
        return None
    df.columns = [str(column.name) for column in table.columns]

    return df


def sqlquery_to_dataframe(results, sqlmodel, index=None, drop=[]):
    """
    Converts a sqlalchemy query to a pandas dataframe

    :rtype : pd.DataFrame
    :param results: sqlachemy.QueryResult
    :param sqlmodel: sqlalchemy.Model
    :param index: Index column of the returned DataFrame
    :param drop: If any columns should be dropped
    :return:
    """
    columns = sqlmodel.columns
    colnames = [str(column.name) for column in columns]
    # Collate all results into dict of lists
    all_results = {colname: [] for colname in colnames}
    for result in results:
        result_dict = result.__dict__
        for colname in colnames:
            if result_dict[colname]:
                all_results[colname].append(result_dict[colname])
            else:
                all_results[colname].append(None)

    df = pd.DataFrame.from_dict(all_results)
    if index is not None:
        df = df.set_index(index)
    for col in drop:
        df = df.drop(col, axis=1)

    return df


def query_to_df(query, index=None, drop=None):
    total_query = query.all()
    if len(total_query) is 0:
        return None
    columns = [str(c['name']) for c in query.column_descriptions]

    df = pd.DataFrame(total_query, columns=columns)

    return df


def to_supported_types(dataframe):
    """ Converts pd.DataFrame types to simple types

    :type dataframe: pandas.core.frame.DataFrame
    :return: DataFrame with python types
    :rtype: pd.DataFrame

    :note: Can often help with windows type issues, e.g: object->str,
        int64->float

    """
    for i in range(len(dataframe.columns)):

        if str(dataframe.dtypes[i]) in "int64":
            dataframe[dataframe.columns[i]] = dataframe[dataframe.columns[i]] \
                .astype(float)
        if str(dataframe.dtypes[i]) in "int32":
            dataframe[dataframe.columns[i]] = dataframe[dataframe.columns[i]] \
                .astype(float)

        # if str(dataframe.dtypes[i]) in "object":
        #     dataframe[dataframe.columns[i]] = dataframe[dataframe.columns[i]] \
        #         .astype(str)

    return dataframe


def insert_pandas(df, table, db, ignore_integrity=True, debug=False):
    """ Insert pandas DataFrame into a database

    :param df: DataFrame for insertion
    :param table: SQLAlchemy table object
    :param db: SQLAlchemy database object

    """
    insert = table.insert()
    for i in range(len(df.axes[0])):
        if ignore_integrity:
            try:
                insert = insert.values(df.iloc[i, :].to_dict())
                db.session.execute(insert)
            except sqlalchemy.exc.IntegrityError as sqla_error:
                if debug is True:
                    print(sqla_error)
                pass
        else:
            insert = insert.values(df.iloc[i, :].to_dict())
            db.session.execute(insert)

    db.session.commit()


def safe_insert_from_csv(table, csv, db, **kwargs):
    """Insert from csv into a database

    :param table: SQLAlchemy core Table
    :param csv: Relative location of csv from projects current directory
    :param db: SQLAlchemy database
    :param kwargs: Arguments for the pandas .csv parser

    """

    if db.session.execute(table.select(use_labels=True)).first() is None:
        insert_table = table.insert(use_labels=True)

        df = pd.read_csv(csv, **kwargs)

        df = to_supported_types(df)
        insert_pandas(df, insert_table, db)


def multi_level_query(table, web_dict, db):
    """Builds a multilevel query object from a dictionary

    :type table: sqlalchemy.sql.schema.Table
    :param table: sqlalchemy core table
    :param web_dict: Dictionary representing key value search pairs
    :param db: sqlalchemy database

    """
    q = db.session.query(table)
    for attr, value in web_dict.items():
        q = q.filter(getattr(table, attr).like("%%%s%%" % value))

    return q


def sqltable_from_dataframe(metadata, data, name, primary_key=None):
    """ Return a SQLAlchemy table from the metadata in a pandas dataframe

    :type primary_key: str
    :param metadata: The database metadata object
    :param data: All data in pandas DataFrame
    :param name: The name of the resulting table
    :type metadata: sqlalchemy.Metadata
    :type data: pd.dataframe
    :type name: str
    :rtype : Table

    """
    type_mapping = {'int64': BigInteger, 'object': String,
                    'float64': Float, 'datetime64[ns]': DateTime}
    cols = list()

    for i in range(len(data.columns)):
        if data.columns[i] is primary_key:
            cols.append(Column(data.columns[i], BigInteger, primary_key=True))
        else:
            cols.append(
                Column(data.columns[i], type_mapping[str(data.dtypes[i])]))

    return Table(name, metadata, *cols)


def get_orm_colnames(orm, session):
    """ Get the column names of an ORM object

    :param orm: ORM Declarative base class
    :param session: SQLAlchemy session object

    :return: Column names
    :rtype: list

    """

    keys = session.execute(session.query(orm))._metadata.keys
    prefix = orm.__tablename__ + '_'
    keys = [k.replace(prefix, '') for k in keys]

    return keys


def attribute_names(cls):
    return [prop.key for prop in class_mapper(cls).iterate_properties
            if isinstance(prop, ColumnProperty)]



def safe_convert_type(df, columns, type_func):
    converted_df = copy.deepcopy(df)
    def convert_func(item):
        if item is not None and pd.notnull(item):
            return type_func(item)
    if not isinstance(columns, list) or isinstance(columns, tuple):
        columns = [columns]
    for column in columns:
        converted_df[column] = \
            converted_df[column].apply(convert_func, convert_dtype=False)

    return converted_df

def build_mapper_query(mappers, session):
    query_args = []
    join_args = []
    filters = []
    for m in mappers:
        query_args.append(m.original_column.label(m.excel_column))
        if m.reference_column is not None:
            f = m.original_column.table.c.id == m.reference_column
            join_args.append((m.original_column.class_, f))#.label(m.excel_column))
            filters.append(f)
    query = session.query(*query_args)
    for j in join_args:
        query = query.outerjoin(*j)

    return query

def delta_hours(delta):
    return delta.total_seconds()/3600

def delta_days(delta):
    return delta.total_seconds()/3600/24

def delta_string(delta):
    if delta.days < 0:
        return '- ' + str(timedelta() - delta)
    return str(delta)

def rand_currency(number):
    return format_currency(Decimal(number), 'R')

def zar_currency(number):
    return format_currency(Decimal(number), 'ZAR')

def usd_currency(number):
    return format_currency(Decimal(number), 'USD')
