from __future__ import print_function
import numpy as np
from sklearn.decomposition import PCA
# from matplotlib import pyplot as plt
import pandas as pd
from sklearn import preprocessing
import os
import scipy


def anynull(item):
    if item is None:
        return True
    elif np.isnan(item):
        return True
    else:
        return False


class PCAMonitorModel(object):

    def __init__(self, n_components):
        self.model = PCA(n_components)

    def fit(self, X):
        self.model.fit(X)

    def predict(self, X):
        transformed_data = self.model.transform(X)
        # Reconstruct from lower dimensions
        reconstruct = self.model.inverse_transform(transformed_data)
        # Analysing residual gives nature of fault
        SSPE = np.sum(np.square(reconstruct - X), axis=1)
        return reconstruct, SSPE, transformed_data


class MSETMonitorModel():

    def __init__(self, ntrain):
        self.ntrain = ntrain
        self.Xtrain = []
        self.inverse_traindist = []
        self.preprocessor = preprocessing.MinMaxScaler()

    def fit(self, X):
        """Trains the autoassociative model"""
        Xtrain = self.preprocessor.fit_transform(X)
        vectors = minmax_vector_norm_ordering(Xtrain, self.ntrain)
        self.Xtrain = Xtrain[vectors, :]
        self.inverse_traindist = np.linalg.inv(auto_kernel(self.Xtrain))

    def predict(self, X):
        """Gets the expected values with respect to the training data"""
        X = self.preprocessor.transform(X)
        similarity_matrix = kernel(self.Xtrain, X)
        reconstruct = self.Xtrain.T.dot(
            (self.inverse_traindist)).dot(similarity_matrix).T
        SSPE = np.sum(np.square(reconstruct - X), axis=1)
        return self.preprocessor.inverse_transform(reconstruct), SSPE


def auto_kernel(X):
    """ Auto associative student T kernel """
    from scipy.spatial.distance import pdist, squareform
    pairwise_dists = squareform(pdist(X, 'euclidean'))
    # fast implemntation of student T kerneal
    return 1 / (1 + np.sqrt(pairwise_dists))


def kernel(X1, X2):
    """The student T kernel between two matrixes"""
    from scipy.spatial.distance import cdist
    pairwise_dists = cdist(X1, X2, 'euclidean')
    # fast implemntation of student T kerneal
    return 1 / (1 + np.sqrt(pairwise_dists))


def minmax_vector_norm_ordering(X, ntrainextra=0):
    """ minmax vector norm ordering
    Args:
       X (float):  The training data
       ntrainextra:The amount of extra datapoints over and above the minmax
    Returns:
        A vector containing the indices of the selected data

    """
    n, m = X.shape

    maxlist = list(np.argmax(X, axis=0))
    minlist = list(np.argmin(X, axis=0))

    norm_x = np.linalg.norm(X, axis=1)
    norm_x = np.sort(norm_x) - min(norm_x)
    diff = (max(norm_x)) / ntrainextra
    selected_points = list()
    count = 0
    for i in range(n):
        if (count * diff < norm_x[i]) & ((count + 1) * diff > norm_x[i]):
            count = count + 1
            selected_points.append(i)

    return np.array(list(set(selected_points + maxlist
                             + minlist))).astype('int64')


def exponential_mean(signal, alpha=0.5):
    """ Exponetialy wheighted moving average
    Args:
       alpha (float):  The degree of smoothing required
    .. note::
       This method uses a sample and hold strategy to remove nans
    """
    if anynull(signal[0]):
        signal[0] = np.nanmean(signal)
    for i in range(signal.size):
        if i == signal.size - 2:
            return signal
        if i == 0:
            continue
        if anynull(signal[i + 1]):
            # Sample and hold strategy of dealing with nans
            signal[i + 1] = signal[i]
            if anynull(signal[i]) & i > 2:
                signal[i + 1] == signal[i + 2]

        signal[i + 1] = signal[i + 1] * (1 - alpha) + signal[i] * alpha


def adaptive_threshold(diag_signal, thresh, latching=0, alpha=0.5):
    """
    Performs thresholding with the additional options of latching and
    an expontial moving average
    Args:
       diag_signal (float):  A vector containing the diagnostic signal
       thresh:The limit detection threshold
    Returns:
        A logical vector containing when the diag_signal
        is above the threshold
    """

    quality = np.zeros(diag_signal.size)
    diag_signal = exponential_mean(diag_signal, alpha)
    if latching == 0:
        for i in range(diag_signal.size):
            if(diag_signal[i] > thresh):
                quality[i] = 1
        return quality

    for i in range(diag_signal.size):
        if i > latching:
            if (diag_signal[i - latching:i] > thresh).sum() == latching:
                quality[i - latching:i] = 1

    return quality


def detect_no_variance(df):
    """ Set bad quality for regions of low variance """

    #df_variance = pd.stats.moments.rolling_var(df, 10)
    df_variance = pd.stats.moments.rolling_var(
        df.resample("1h", fill_method="ffill"), window=10, min_periods=1)

    #df_variance.values=np.divide(1, df_variance.values)
    maxvar = np.max(df_variance['values'].values)
    invert = lambda x2: np.abs(x2 - maxvar)

    df_variance["values"] = df_variance["values"].apply(invert)

    return adaptive_threshold(df_variance["values"].values, maxvar * 0.999, latching=50, alpha=0.1)


def vec(num):
    """ Force numpy 0-D vectors into row matrices """
    if len(num.shape) == 1:
        list_num = np.array([num]).T
    else:
        list_num = np.array(num)
    return list_num