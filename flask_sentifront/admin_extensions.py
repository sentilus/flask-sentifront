import wtforms
from flask import url_for, request, flash, send_file, redirect
from flask.ext.admin import BaseView, expose
from flask.ext.security import current_user
import xlsxwriter
import os

class BulkUploadForm:
    file_upload = wtforms.FileField('Select a file field')


class BulkUpload(BaseView):
    """ Bulk Upload of excel data

    Admin view for uploading excel data to a database. Redirects to a given
    model page or error log upon upload.

    """

    def __init__(self, collector=None, success_redirect='/admin/',
                 download_file=None, *args, **kwargs):
        """ Constructor

        :param collector: iExcelCollector.
            Collector object with methods to read, insert and return errors for
            an excel workbook
        :param success_redirect: str.
            Where to redirect the user upon successful upload
        :param download_file: str.
            Location of the template file for download. If None, removes the
            template download link
        :param args:
            Additional arguments for admin view
        :param kwargs:
            Additional keyword arguments for admin view

        """
        super(BulkUpload, self).__init__(*args, **kwargs)
        self.collector = collector
        self.success_redirect = success_redirect
        self.download_file = download_file

    def is_accessible(self):
        return current_user.is_authenticated()

    @expose('/')
    def admin_upload(self):
        return self.render(
            'admin_extensions/bulk_upload.html', upload_header=self.name,
            form=BulkUploadForm, download_url=url_for('.download'),
            upload_url=url_for('.upload'))

    @expose('/upload/', methods=['GET', 'POST'])
    def upload(self):
        errors = None
        if request.method == 'POST':
            file = request.files['file']
            collector_inst = self.collector()
            collector_inst.collect(file)
            collector_inst.replace_insert()
            errors = collector_inst._get_errors()

        if errors:
            return self.render('admin_extensions/upload_errors.html',
                               errors=errors)
        else:
            flash('Successful Upload', 'message')
            return redirect(self.success_redirect)

    @expose('/download/')
    def download(self):
        curr_dir = os.path.dirname(__file__)
        file = curr_dir + "\DatabaseDump.xlsx" #self.download_file
        workbook = xlsxwriter.Workbook(file)
        collector_inst= self.collector()
        workbook = collector_inst.create_excel(None,workbook,collector_inst.orm.__name__)

        workbook.close()

        return send_file(file, as_attachment=True, attachment_filename=self.download_file)
