from flask import Blueprint
from flask.ext.security import Security, SQLAlchemyUserDatastore, UserMixin, \
    RoleMixin, RegisterForm
from wtforms import TextField
from wtforms.validators import Required


class Frontend(object):
    """ Base class of the front extension

        Can be initialised on the app in one of two ways:

        >>> frontend = Frontend(app, db)

        or

        >>> frontend = Frontend()
        >>> frontend.init_app(app, db)

    """

    def __init__(self, app=None, db=None, **kwargs):
        """ Constructor

            :type app: flask.Flask
            :param app: Application object
            :type db: flask.ext.sqlalchemy.SQLAlchemy
            :param db: Flask-SQLAlchemy object linked to the app

        """
        for k, v in kwargs.items():
            setattr(self, k, v)

        if app is not None:
            assert db is not None, "db required to initialise extension"
            self.init_app(app, db)

    def init_app(self, app, db, mail=None, login_manager=None, user=None,
                 role=None, user_role=None, register_form=None):
        if user_role is None:
            roles_users = self.default_user_role(db)
        if user is None:
            user = self.default_user(db, roles_users)
        if role is None:
            role = self.default_role(db)
        if register_form is None:
            register_form = self.default_register_form()

        sentifront = self.create_blueprint(
            "sentifront", template_folder="templates",
            static_url_path="/static/sentifront", static_folder="static")
        app.register_blueprint(sentifront)

        admin_extensions = self.create_blueprint(
            "admin_extensions", template_folder="templates")
        app.register_blueprint(admin_extensions)

        sentidash = self.create_blueprint(
            "sentidash", template_folder="templates",
            static_folder="static/sentidash")
        app.register_blueprint(sentidash)

        return app

    def init_sentidash(self, app):

        sentidash = self.create_blueprint(
            "sentidash", template_folder="templates",
            static_folder="static/sentidash")
        app.register_blueprint(sentidash)

        return app

    def add_view(self, view):
        self.app.register_blueprint(view.create_blueprint)

    def default_user(self, db, roles_users):

        class User(db.Model, UserMixin):
            id = db.Column(db.Integer, primary_key=True)
            email = db.Column(db.String(255), unique=True)
            password = db.Column(db.String(255))
            active = db.Column(db.Boolean())
            confirmed_at = db.Column(db.DateTime())

            roles = db.relationship('Role', secondary=roles_users,
                                    backref=db.backref('users',
                                                       lazy='dynamic'))

        return User

    def default_role(self, db):

        class Role(db.Model, RoleMixin):
            id = db.Column(db.Integer(), primary_key=True)
            name = db.Column(db.String(80), unique=True)
            description = db.Column(db.String(255))

        return Role

    def default_user_role(self, db):

        roles_users = db.Table('roles_users',
                               db.Column('user_id', db.Integer(),
                                         db.ForeignKey('user.id')),
                               db.Column('role_id', db.Integer(),
                                         db.ForeignKey('role.id')))

        return roles_users

    def default_register_form(self):

        class NameRegisterForm(RegisterForm):
            name = TextField('Name', validators=[Required()])

        return NameRegisterForm

    def create_blueprint(self, blueprint_name, **kwargs):
        module = Blueprint(blueprint_name, __name__, **kwargs)
        return module

