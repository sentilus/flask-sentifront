import itertools
import time
import datetime

from wtforms import fields
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
# from flask.ext.admin.contrib.sqla.fields import QuerySelectField, QuerySelectMultipleField
from flask.ext.admin.babel import gettext
from flask.ext.admin._compat import text_type, as_unicode
# from flask.ext.admin.model.fields import InlineFieldList
from flask.ext.admin.contrib.sqla.tools import get_primary_key

from . import widgets as senti_widgets
from .widgets import InlineFieldListWidget, InlineFormWidget, ModelFormWidget
from wtforms.widgets import HTMLString, html_params
import six



class DateField(fields.DateField):

    widget = senti_widgets.DatePickerWidget()

    def __init__(self, label=None, validators=None, format=None, **kwargs):

        super(DateField, self).__init__(label, validators, **kwargs)

        self.format = format or '%Y-%m-%d'


class DateTimeField(fields.DateTimeField):
    """
       Allows modifying the datetime format of a DateTimeField using form_args.
    """
    widget = senti_widgets.DateTimePickerWidget()
    def __init__(self, label=None, validators=None, format=None, **kwargs):
        """
            Constructor

            :param label:
                Label
            :param validators:
                Field validators
            :param format:
                Format for text to date conversion. Defaults to '%Y-%m-%d %H:%M:%S'
            :param kwargs:
                Any additional parameters
        """
        super(DateTimeField, self).__init__(label, validators, **kwargs)

        self.format = format or '%Y-%m-%d %H:%M:%S'

class TimeField(fields.Field):
    """
        A text field which stores a `datetime.time` object.
        Accepts time string in multiple formats: 20:10, 20:10:00, 10:00 am, 9:30pm, etc.
    """
    widget = senti_widgets.TimePickerWidget()

    def __init__(self, label=None, validators=None, formats=None,
                 default_format=None, widget_format=None, **kwargs):
        """
            Constructor

            :param label:
                Label
            :param validators:
                Field validators
            :param formats:
                Supported time formats, as a enumerable.
            :param default_format:
                Default time format. Defaults to '%H:%M:%S'
            :param kwargs:
                Any additional parameters
        """
        super(TimeField, self).__init__(label, validators, **kwargs)

        self.formats = formats or ('%H:%M:%S', '%H:%M',
                                   '%I:%M:%S%p', '%I:%M%p',
                                   '%I:%M:%S %p', '%I:%M %p')

        self.default_format = default_format or '%H:%M:%S'

    def _value(self):
        if self.raw_data:
            return u' '.join(self.raw_data)
        else:
            return self.data and self.data.strftime(self.default_format) or u''

    def process_formdata(self, valuelist):
        if valuelist:
            date_str = u' '.join(valuelist)

            for format in self.formats:
                try:
                    timetuple = time.strptime(date_str, format)
                    self.data = datetime.time(timetuple.tm_hour,
                                              timetuple.tm_min,
                                              timetuple.tm_sec)
                    return
                except ValueError:
                    pass

            raise ValueError(gettext('Invalid time format'))


class Select2QueryField(QuerySelectField):

    """ Sets the widget for a query select field to select2 """

    widget = senti_widgets.Select2Widget()


class Select2TagsQueryField(QuerySelectMultipleField):

    """ Sets the widget for a multiple query select field to select2 tags """

    widget = senti_widgets.Select2TagsWidget(multiple=True)


class Select2QueryAjaxField(QuerySelectField):

    """ Sets the widget for a query select field to select2 """
    widget = senti_widgets.Select2WidgetAjax()

    def __init__(self, *args, **kwargs):
        if 'href' in kwargs:
            self.widget = senti_widgets.Select2WidgetAjax(href=kwargs.pop('href'))

        super(Select2QueryAjaxField, self).__init__(*args, **kwargs)
    
class Select2QueryAjaxTagsField(QuerySelectField):

    """ Sets the widget for a query select field to select2 """

    widget = senti_widgets.Select2WidgetAjaxTags()




class InlineFieldList(fields.FieldList):

    """ Field for list for fields for Inline Fields """

    widget = InlineFieldListWidget()

    def __init__(self, *args, **kwargs):
        super(InlineFieldList, self).__init__(*args, **kwargs)

        # Create template
        self.template = self.unbound_field.bind(form=None, name='')

        # Small hack to remove separator from FormField
        if isinstance(self.template, fields.FormField):
            self.template.separator = ''

        self.template.process(None)

    def __call__(self, **kwargs):
        return self.widget(self,
                           template=self.template,
                           check=self.display_row_controls,
                           **kwargs)

    def display_row_controls(self, field):
        return True

    def process(self, formdata, data=None):
        res = super(InlineFieldList, self).process(formdata, data)

        # Postprocess - contribute flag
        if formdata:
            for f in self.entries:
                key = 'del-%s' % f.id
                f._should_delete = key in formdata

        return res

    def validate(self, form, extra_validators=tuple()):
        """
            Validate this FieldList.

            Note that FieldList validation differs from normal field validation in
            that FieldList validates all its enclosed fields first before running any
            of its own validators.
        """
        self.errors = []

        # Run validators on all entries within
        for subfield in self.entries:
            if not self.should_delete(subfield) and not subfield.validate(form):
                self.errors.append(subfield.errors)

        chain = itertools.chain(self.validators, extra_validators)
        self._run_validation_chain(form, chain)

        return len(self.errors) == 0

    def should_delete(self, field):
        return getattr(field, '_should_delete', False)

    def populate_obj(self, obj, name):
        values = getattr(obj, name, None)
        try:
            ivalues = iter(values)
        except TypeError:
            ivalues = iter([])

        candidates = itertools.chain(ivalues, itertools.repeat(None))
        _fake = type(str('_fake'), (object, ), {})

        output = []
        for field, data in zip(self.entries, candidates):
            if not self.should_delete(field):
                fake_obj = _fake()
                fake_obj.data = data
                field.populate_obj(fake_obj, 'data')
                output.append(fake_obj.data)

        setattr(obj, name, output)


class InlineFormField(fields.FormField):
    """
        Inline version of the ``FormField`` widget.
    """
    widget = InlineFormWidget()


class InlineModelFormField(fields.FormField):
    """
        Customized ``FormField``.

        Excludes model primary key from the `populate_obj` and
        handles `should_delete` flag.
    """
    widget = InlineFormWidget()

    def __init__(self, form_class, pk='id', form_opts=None, **kwargs):
        super(InlineModelFormField, self).__init__(form_class, **kwargs)

        self._pk = pk
        self.form_opts = form_opts

    def get_pk(self):
        return getattr(self.form, self._pk).data

    def populate_obj(self, obj, name):
        for name, field in six.iteritems(self.form._fields):
            if name != self._pk:
                field.populate_obj(obj, name)


class ModelFormField(fields.FormField):
    """
    One-to-one model form
    """

    widget = ModelFormWidget()

    def __init__(self, form_class, model, session, parent_rel=None, **kwargs):
        super(ModelFormField, self).__init__(form_class, **kwargs)

        self._pk = get_primary_key(model)
        self.model = model
        self.session = session
        self.parent_rel = parent_rel

    def get_pk(self):
        return getattr(self.form, self._pk).data

    def populate_obj(self, obj, name):
        value = getattr(obj, self.parent_rel)

        model_args = {}

        for name, field in self.form._fields.iteritems():
            if name != self._pk:
                model_args.update({name: field.data})

        model = self.model(**model_args)
        self.session.add(model)

        setattr(obj, self.parent_rel, model)


class InlineModelFormList(InlineFieldList):
    """
        Customized inline model form list field.
    """

    form_field_type = InlineModelFormField
    """
        Form field type. Override to use custom field for each inline form
    """

    def __init__(self, form, session, model, prop, **kwargs):
        """
            Default constructor.

            :param form:
                Form for the related model
            :param session:
                SQLAlchemy session
            :param model:
                Related model
            :param prop:
                Related property name
            :param inline_view:
                Inline view
        """
        self.form = form
        self.session = session
        self.model = model
        self.prop = prop

        self._pk = get_primary_key(model)
        form_field = self.form_field_type(form, self._pk)

        super(InlineModelFormList, self).__init__(form_field, **kwargs)

    def display_row_controls(self, field):
        return field.get_pk() is not None

    def populate_obj(self, obj, name):
        values = getattr(obj, name, None)

        if values is None:
            return

        # Create primary key map
        pk_map = dict((str(getattr(v, self._pk)), v) for v in values)

        # Handle request data
        for field in self.entries:
            field_id = field.get_pk()

            if field_id in pk_map:
                model = pk_map[field_id]

                if self.should_delete(field):
                    # self.session.delete(model)
                    values.remove(model)
                    continue
            else:
                model = self.model()
                values.append(model)

            field.populate_obj(model, None)
