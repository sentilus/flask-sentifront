from wtforms import widgets
from flask.globals import _request_ctx_stack
from flask.ext.admin.babel import gettext, ngettext
from flask.ext.admin import helpers as h
from wtforms.widgets import HTMLString, html_params

class DateTimePickerWidget(widgets.TextInput):
    """
        Datetime picker widget.

        You must include bootstrap-datepicker.js and form.js for styling to work.
    """
    def __call__(self, field, **kwargs):
        kwargs.setdefault('data-role', u'datetimepicker')

        kwargs.setdefault('data-date-format', u'YYYY-MM-DD HH:mm:ss')
        # kwargs.setdefault('data-date-useseconds', u'false')
        kwargs.setdefault('data-date-today-btn', u'linked')
        kwargs.setdefault('data-date-today-highlight', u'true')

        kwargs.setdefault('data-date-autoclose', u'true')
        kwargs.setdefault('class', u'form-control')
        return super(DateTimePickerWidget, self).__call__(field, **kwargs)


class TimePickerWidget(widgets.TextInput):
    """
        Date picker widget.

        You must include bootstrap-datepicker.js and form.js for styling to work.
    """
    def __call__(self, field, **kwargs):
        kwargs.setdefault('data-role', u'timepicker')

        kwargs.setdefault('data-date-format', u'hh:mm:ss')

        kwargs.setdefault('data-date-autoclose', u'true')
        return super(TimePickerWidget, self).__call__(field, **kwargs)


class DatePickerWidget(widgets.TextInput):
    """
        Date picker widget.

        You must include bootstrap-datepicker.js and form.js for styling to work.
    """
    def __call__(self, field, **kwargs):
        kwargs.setdefault('data-role', u'datepicker')

        kwargs.setdefault('data-date-format', u'YYYY-MM-DD')
        kwargs.setdefault('class', u'form-control')

        kwargs.setdefault('data-date-autoclose', u'true')
        self.date_format = kwargs['data-date-format']
        return super(DatePickerWidget, self).__call__(field, **kwargs)


class Select2Widget(widgets.Select):
    """
        `Select2 <https://github.com/ivaynberg/select2>`_ styled select widget.

        You must include select2.js, form.js and select2 stylesheet for it to
        work.
    """
    def __call__(self, field, **kwargs):
        kwargs.setdefault('data-role', u'select2')
        kwargs.setdefault('style', 'width: 70%')

        allow_blank = getattr(field, 'allow_blank', False)
        if allow_blank and not self.multiple:
            kwargs['data-allow-blank'] = u'1'

        return super(Select2Widget, self).__call__(field, **kwargs)


class Select2WidgetAjax(widgets.Select):
    extra_classes = None

    def __init__(self, extra_classes=None,href=''):
        self.extra_classes = extra_classes
        self.href=href
        return super(Select2WidgetAjax, self).__init__()

    def __call__(self, field, **kwargs):
        if self.extra_classes:
            kwargs['class'] = u'my_select2_ajax form-control ' + self.extra_classes
        else:
            kwargs['class'] = u'my_select2_ajax form-control'

        if self.href:
            if hasattr(self.href, '__call__'):
                kwargs['href'] = self.href()
            else:
                kwargs['href'] = self.href

        kwargs['style'] = u'width:250px'
        kwargs['data-placeholder'] = u'Select Value'
        if 'name_' in kwargs:
            field.name = kwargs['name_']

        kwargs.setdefault('id', field.id)
        if self.multiple:
            kwargs['multiple'] = True
        
        #if False:
        #    html = ['<select %s>' % html_params(name=field.name, **kwargs)]
        #    for val, label, selected in field.iter_choices():
        #        html.append(self.render_option(val, label, selected))
        #    html.append('</select>')
        #else:

        if field.data is not None:
            value=str(field.data.id)
            #value='{"id":"%s","text":"%s"}' %(str(field.data.id), field.data)
        else:
            value=""
        html = ['<input %s type = "hidden">' % html_params(name=field.name,value=value, **kwargs)]
       
            # for val, label, selected in field.iter_choices():
            #             html.append(self.render_option(val, label, selected))
        html.append('</input>')

        return HTMLString(''.join(html))


class Select2WidgetAjaxTags(widgets.Select):
    extra_classes = None

    def __init__(self, extra_classes=None):
        self.extra_classes = extra_classes
        return super(Select2WidgetAjaxTags, self).__init__()

    def __call__(self, field, **kwargs):
        if self.extra_classes:
            kwargs['class'] = u'my_select2_ajax_tags form-control ' + self.extra_classes
        else:
            kwargs['class'] = u'my_select2_ajax_tags form-control'
        kwargs['style'] = u'width:250px'
        kwargs['data-placeholder'] = u'Select Value'
        if 'name_' in kwargs:
            field.name = kwargs['name_']

        kwargs.setdefault('id', field.id)
        if self.multiple:
            kwargs['multiple'] = True
        
        #if False:
        #    html = ['<select %s>' % html_params(name=field.name, **kwargs)]
        #    for val, label, selected in field.iter_choices():
        #        html.append(self.render_option(val, label, selected))
        #    html.append('</select>')
        #else:

        if field.data is not None:
            value=str(field.data.id)
            #value='{"id":"%s","text":"%s"}' %(str(field.data.id), field.data)
        else:
            value=""
        html = ['<input %s type = "hidden">' % html_params(name=field.name,value=value, **kwargs)]
       
            # for val, label, selected in field.iter_choices():
            #             html.append(self.render_option(val, label, selected))
        html.append('</input>')

        return HTMLString(''.join(html))

class Select2TagsWidget(widgets.Select):
    """`Select2 <http://ivaynberg.github.com/select2/#tags>`_ styled text widget.
    You must include select2.js, form.js and select2 stylesheet for it to work.
    """
    def __call__(self, field, **kwargs):
        kwargs.setdefault('data-role', u'select2')
        kwargs.setdefault('style', 'width: 70%')
        return super(Select2TagsWidget, self).__call__(field, **kwargs)


class RenderTemplateWidget(object):
    """
        WTForms widget that renders Jinja2 template
    """
    def __init__(self, template):
        """
            Constructor

            :param template:
                Template path
        """
        self.template = template

    def __call__(self, field, **kwargs):
        ctx = _request_ctx_stack.top
        jinja_env = ctx.app.jinja_env

        kwargs.update({
            'field': field,
            '_gettext': gettext,
            '_ngettext': ngettext,
            'h': h,
        })

        template = jinja_env.get_template(self.template)
        return template.render(kwargs)


class InlineFieldListWidget(RenderTemplateWidget):
    def __init__(self):
        super(InlineFieldListWidget, self).__init__('sentidash/inline_field_list.html')


class InlineFormWidget(RenderTemplateWidget):
    def __init__(self):
        super(InlineFormWidget, self).__init__('sentidash/inline_form.html')

    def __call__(self, field, **kwargs):
        kwargs.setdefault('form_opts', getattr(field, 'form_opts', None))
        return super(InlineFormWidget, self).__call__(field, **kwargs)


class ModelFormWidget(RenderTemplateWidget):
    def __init__(self):
        super(ModelFormWidget, self).__init__('sentidash/model_form.html')
